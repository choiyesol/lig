$(function(){
    // 01-3. root, page명 체킹
	pagechecking()

    // 메인 슬라이드 -DNI 서영기
    reMainSlideAct();

    //#businessMain 멀티슬라이드 -DNI 최예솔
    moveBusinessSlide() ;

    //하단슬라이드 -DNI 최예솔
    bottomSlide ();

    // 메인 LIG소식 소팅 -DNI 서영기
    reMainVideoNews();

    // 07. 메뉴구조적용 - DNI 서영기
    buildMenuList();
}); 


// 01-3. root, page명 체킹
// DNI 서영기
// 최초 페이지 로드시 현재 루트를 및 언어 설정을 확인한다.
var lengType = '';
function pagechecking() {	
	var locationArray = document.location.pathname.split('/')
	var checkRootPosition = 0;
	var checkPagePosition = 0;
	if(locationArray.length == 6) {
		checkRootPosition = locationArray.length-3
		checkPagePosition = locationArray.length-2
	} else {
		checkRootPosition = locationArray.length-2
		checkPagePosition = locationArray.length-1
	}
	lengType = locationArray[2]	
	pageRoot = locationArray[checkRootPosition];
	
	var fileNameArray = locationArray[checkPagePosition].split('.');
	pageLocation = fileNameArray[0]	
}


// 05. 메인 슬라이드 - 범용 세팅(X)
// DNI 서영기
// 기본 슬라이드 세팅 li기준 자동 인디케이터 및 width 생성
function reMainSlideAct(){
    var reMainSlideTime = 10000;
    var reMainSlideSpeed = 240;
    var reMainSlideInterval = null;
    var reMainSlideAct = true;

    // 초기 슬라이드 영역 세팅
    // 기본 슬라이드와 별개의 동작 세팅
    var moveArea = $('.mainSlideArea > div');
    var indicatorArea = $('.mainSlideArea > ol.indicator');
	moveArea.children('ul').css({'width' : (moveArea.find('li').length * 100) + 'vw'});
    moveArea.find('li').each(function(i){
        var indexNumber = i;
        if(indexNumber < 10) {
            indexNumber = '0'+ (i+1)
        }

        var dataSeq = $(this).attr('data-seq');
        var indicatorList = '<li data-seq="' + dataSeq + '">\n';                
        indicatorList += '<a href="javascript:;">' + indexNumber + '<div><span></span></div></a>\n';
        indicatorList += '</li>\n';
        indicatorArea.append(indicatorList);
    })
	// 슬라이드 영역 버튼 width정리
	// pc - indicatorArea 기본 leftmargin = margin-left: calc(50% - 661px);
	// mobile - indicatorArea 기본 leftmargin = margin-left: calc(50% - 158px);
    // mobile - button:eq(1) 겹치는 여백 제외한 값으로 기본 leftmargin = margin-left: calc(50% - 150px);
	indicatorArea.find('li').first().addClass('on')
	$('.mainSlideArea > button:eq(1)').css({'margin-left': 'calc(45px + '+ indicatorArea.outerWidth() +'px)'})
	$('.mainSlideArea > button.stopPlay').css({'margin-left': 'calc(45px + '+ (24 + indicatorArea.outerWidth()) +'px)'})
    intervalMainSlide();
    reMainIndicatorAct();
    reMainSlideBind();

    function intervalMainSlide() {
        if(reMainSlideInterval == null) {
            reMainSlideInterval = setInterval(function(){
                moveMainSlide('next');                    
            }, reMainSlideTime)
        }
    }

    function stopMainSlide() {
        clearInterval(reMainSlideInterval);
        reMainSlideInterval = null;
    }

    //unbinded = boolean 
    //바인드 여부를 결정한다.
    function reMainSlideBind(unbinded) {
        if(!unbinded) {
            $('.mainSlideArea > button:not(".stopPlay")').on({
                'click': function(){
                    stopMainSlide()
                    if($(this).index() == 1) {
                        moveMainSlide('prev');
                    } else if($(this).index() == 2) {
                        moveMainSlide('next');
                    }
                    if(reMainSlideAct) {
                        intervalMainSlide()
                    }
                }
            })
			$('.mainSlideArea > button.stopPlay').on({                
                'click': function(){
                    if($(this).hasClass('on')){
						reMainSlideAct = true;
						$(this).removeClass('on')
						intervalMainSlide();
						reMainIndicatorAct();
						$(this).html('재생하기')
					}else {
						reMainSlideAct = false;
						$(this).addClass('on')
						stopMainSlide();
						$(this).html('멈춤')
					}
				}
            })
			$('.mainSlideArea > button.nextArea').on({                
                'click': function(){
					var moveArea = $('#businessMain').offset().top - 90;					
                    $('body, html').animate({'scrollTop': moveArea}, 180)
				}
            })
            indicatorArea.find('li').on({
                'click': function(){
                    stopMainSlide()                            
                    moveMainSlide(Number($(this).attr('data-seq')));
                    if(reMainSlideAct) {
                        intervalMainSlide()
                    }
                }
            })
        } else {
            $('.mainSlideArea > button').off('click')
            indicatorArea.find('li').off('click')
        }
    }

    //stopCheck = boolean
    function reMainIndicatorAct(stopCheck) {                
        var checkSeq = $('.mainSlideArea > div li').eq(0).attr('data-seq');
        var checkIndex = 0;
        indicatorArea.children('li').each(function(i){
            if($(this).attr('data-seq') == checkSeq){
                checkIndex = $(this).index();
            }
        })            
        indicatorArea.children('li').find('span').clearQueue(false);            
        indicatorArea.children('li').removeClass('on');
        indicatorArea.children('li').find('span').css({'width' : '0'});
        indicatorArea.children('li').eq(checkIndex).addClass('on');
        if(stopCheck){
            indicatorArea.children('li').eq(checkIndex).find('span').css({'width' : '100%'})
        } else {
            indicatorArea.children('li').eq(checkIndex).find('span').animate({'width' : '100%'}, reMainSlideTime)
        }
    }

    function moveMainSlide(dataSeq) {
        stopMainSlide();
        reMainSlideBind(true);
        var moveLeftWidth = $(window).width();
        var afterFunction = '';
        var appended = 1;
        if(typeof(dataSeq) == 'number') {
            moveArea.find('li').each(function(i){                    
                if($(this).attr('data-seq') == dataSeq) {
                    appended = i;
                }
            })
        } else if(dataSeq == 'prev') {                 
            appended = -1;                
        }
        
        var moveTarget = '';
        if(appended == -1) {
            moveArea.scrollLeft(moveLeftWidth)
            moveTarget = moveArea.find('li').last();
            moveTarget.clone().prependTo(moveArea.children('ul'))
            moveTarget.remove();
            moveArea.animate({'scrollLeft' : 0}, reMainSlideSpeed, function(){
                reMainIndicatorAct()
            })
        } else if( appended > 0 ) {                        
            moveArea.animate({'scrollLeft' : appended * moveLeftWidth}, reMainSlideSpeed, function(){                    
                for(var i= 0; i < appended; i++) {                        
                    moveTarget = moveArea.find('li').first();
                    moveTarget.clone().appendTo(moveArea.children('ul'))
                    moveTarget.remove();
                    moveArea.scrollLeft(0);
                }
                reMainIndicatorAct();
            })                
        } else {                
            reMainIndicatorAct();
        }
        if(reMainSlideAct) {
            intervalMainSlide()
        }
        reMainSlideBind();
    }
}

// 01. #businessMain 멀티슬라이드
// DNI 최예솔
function moveBusinessSlide(){
	var productAPI = 'API/productTree.json';
    var busislideArea = $('#businessMain > div');
    var currentPageArea = $('#businessMain > p');
    var imgCnt = 1;

	var ajaxAction = $.get(productAPI, {}, function(data){
		var listHtml = '';
        var pagingHtml = '';
        var indx = data.pages.length;
        var currentindx = '';
		$.each(data.pages, function(i){
            currentindx = i;
			var bgName = 'images_mobile/reMain/img_main' + this.categoryKey
			listHtml += '<li style="background-image: url(\''+ bgName +'.png\')">\n';			
			listHtml += '<a href="javascript:;">\n';
            listHtml += '<div class="contents">\n';
            listHtml += '<div class="imgArea"><img src="images_mobile/reMain/ico_main'+ this.categoryKey +'.png" alt="'+ this.engcategoryName +' 아이콘"></div>\n';
            listHtml += '<h2>'+ this.engcategoryName +'</h2>\n';
            // listHtml += '<p>'+ this.categoryMessage  +'</p>\n';
            listHtml += '<span onclick="top.location.href=\'/web/' + lengType + '/product/product.do?category=' + this.categoryKey +'\'">View more</span>\n';
			listHtml += '</div>\n';
            listHtml += '</a>\n';
            listHtml += '</li>\n';
        
            pagingHtml = '<span>' + imgCnt + '</span>' + ' / '+ data.pages.length;
            currentPageArea.html(pagingHtml);
		})
		busislideArea.find('ul').html(listHtml);
		var listCnt = busislideArea.find('li').length;
		var listWidth = $(window).outerWidth();
		$('#businessMain li').width(listWidth);
		busislideArea.children('ul').css({'width': listCnt * listWidth + 'px'})


	})
    
	ajaxAction.done(function(){        
		buttonBind(true);
		// 버튼 액션 영역 시작		
		var maxScroll = busislideArea.children('ul').outerWidth() - $(window).outerWidth();
		var movebusiSlide = $('#businessMain li').width();
		var busislidTarget = '';
		//direction = prev, next, undefined
		function moveOption(direction){
			var nowScroll = busislideArea.scrollLeft();
			buttonBind()
			if(direction == 'next') {
				if(nowScroll == maxScroll) {
					busislideArea.animate({'margin-left' : '-20px'}, 180, function(){
						busislideArea.animate({'margin-left' : 0}, 60)
						buttonBind(true)                        
					})
				} else {
					busislideArea.animate({'scrollLeft' : nowScroll + movebusiSlide}, 240, function(){
                        imgCnt++;
                        currentPageArea.children('span').html(imgCnt)
						buttonBind(true)
					})
				}			
			} else {
				if(busislideArea.scrollLeft() == 0) {
					busislideArea.animate({'margin-left' : '20px'}, 180, function(){
						busislideArea.animate({'margin-left' : 0}, 60)
						buttonBind(true)
					})					
				} else {
					busislideArea.animate({'scrollLeft' : nowScroll - movebusiSlide}, 240, function(){
                        imgCnt--;
                        currentPageArea.children('span').html(imgCnt)
						buttonBind(true)
					})
				}
			}
		}

		//binded = boolean
		function buttonBind(binded){
			if(binded == true) {
				$('#businessMain > button').on({
					'click' : function(){
						if($(this).hasClass('busibtnPre')){
							moveOption('prev')
						} else {
							moveOption('next')
						}
					}
				})
			} else {
				$('#businessMain > button').off('click')
			}
		}

		var movePoint = 0;
		var firstCheckMargin = 0;
		var checkTouchPoint = 0;

		busislideArea.find('a').on({
			'touchstart' : function(ev){
				buttonBind()				
				checkTouchPoint = ev.originalEvent.changedTouches[0].pageX;
				firstCheckMargin = busislideArea.scrollLeft();
				swipeRun(checkTouchPoint, $(this));
			},
			'touchend' : function(ev){				
				var lastCheckMargin =  busislideArea.scrollLeft();
				checkCnt = lastCheckMargin - firstCheckMargin;
				busislideArea.find('a').off('touchstart touchmove', swipeRun);
				$('body').css({"overflow" : 'auto'})				
				if(checkCnt < -40) {
					busislideArea.animate({'scrollLeft' : firstCheckMargin - movebusiSlide}, 240, function(){
                        imgCnt--;
                        currentPageArea.children('span').html(imgCnt)
						buttonBind(true)
					})					
				} else if(checkCnt > 40 ) {					
					busislideArea.animate({'scrollLeft' : firstCheckMargin + movebusiSlide}, 240, function(){
                        imgCnt++;
                        currentPageArea.children('span').html(imgCnt)
						buttonBind(true)
					})
				} else {
					busislideArea.animate({'scrollLeft' : firstCheckMargin}, 100, function(){
						buttonBind(true)
					})	
				}		
				busislideArea.find('a').on({
					'touchstart' : function(ev){
						checkTouchPoint = ev.originalEvent.changedTouches[0].pageX;		
						firstCheckMargin = busislideArea.scrollLeft();
						swipeRun(checkTouchPoint, $(this));
					}
				})
			}
		})
		var swipeInterval = null;
		function swipeRun(startMove, obj) {
			busislideArea.find('a').on({
				'touchmove' : function(ev) {
					if(swipeInterval == null){
						console.log('aa')
						swipeInterval = setTimeout(function(){
							var moveX = ev.originalEvent.changedTouches[0].pageX;
							var listsMargin = busislideArea.scrollLeft();
							movePoint = checkTouchPoint - moveX;
							if(movePoint < -20 || movePoint > 20) {
								ev.preventDefault();
								$('body').css({"overflow" : 'hidden'})
								busislideArea.scrollLeft(listsMargin + movePoint)
								checkTouchPoint = ev.originalEvent.changedTouches[0].pageX;				
							}
							clearTimeout(swipeInterval)
							swipeInterval = null;
						}, 10)
					}
				}
			})
		}
	})
}

// 03. reMain 하단슬라이드
// DNI 최예솔
function bottomSlide (){
    var slidecounter = $('#slideMain > div > div ul li').length;
    var slideWidth = $(window).outerWidth(); //mobile - 값 변경
    var currentIndex = 0;

    $('#slideMain div.listContainer ul').css({'width' : slideWidth * slidecounter + 'px' });     
    $('#slideMain div.listContainer li').css ({'width' : slideWidth + 'px' }); //mobile - 추가

    creatIndicator();
    clickSlideBtn();
    clickPagingBtn();

    //03-1 SlidIndicator 생성
    function creatIndicator (){
        if(slidecounter >= 2){
            var slideIndicator ='';
            $('#slideMain div.listContainer ul li').each (function(i){
                slideIndicator += '<a href="javascript:;"';
                if(i==0) {
                    slideIndicator += 'class="btnPaging on"';
                } else {
                    slideIndicator += 'class="btnPaging"';
                }
                slideIndicator += '>' + ( i +1) + '번째 이미지</a>'
            });
            $('#slideMain > div .slidIndicator').html(slideIndicator);
        }
    }

    //03-2  SlidIndicator ON
    function goToslide(index){
		//console.log($('#slideMain div.listContainer').scrollLeft())
        $('#slideMain div.listContainer').animate({'scrollLeft': (index * slideWidth)}, 240, function(){
			currentIndex = index;
			$('#slideMain > div .slidIndicator').children('a').removeClass('on');
			$('#slideMain > div .slidIndicator').children('a').eq(currentIndex).addClass('on');
		});
    }

    //03-3 paging 버튼 click
    function clickPagingBtn (){
        $('#slideMain > div .slidIndicator').children('.btnPaging').click(function(){
            var idx = $(this).index();
            goToslide(idx);
        });
    }

    // 03-4 좌우 버튼 (prev, next) click
    function clickSlideBtn(){
        $('#slideMain > div > a').click(function(e){
            e.preventDefault();
            if($(this).hasClass('btnPrev')){
                if(currentIndex == 0){ 
                    $('#slideMain div.listContainer ul').animate({paddingLeft:'40px', opacity:0.3 },240,function(){
                        $(this).animate({paddingLeft: '0',opacity:1 },180);
                    });
                } else {
                    goToslide(currentIndex -1);
                }
            } else{
                if(currentIndex == (slidecounter-1)){
                    $('#slideMain div.listContainer ul li').animate({marginRight:'-25px',opacity:0.3},240,function(){
                        $(this).animate({marginRight: '0',opacity:1 },180);
                    });
                    console.log(currentIndex);
                } else{
                    goToslide(currentIndex + 1);
                }
            }
        });
    }
	var movePoint = 0;
	var firstCheckMargin = 0;
	var checkTouchPoint = 0;
	var slideArea = $('#slideMain  div.listContainer');

	slideArea.find('li').on({
		'touchstart' : function(ev){			
			console.log('aa')
			checkTouchPoint = ev.originalEvent.changedTouches[0].pageX;
			firstCheckMargin = slideArea.scrollLeft();
			swipeRun(checkTouchPoint, $(this));
		},
		'touchend' : function(ev){
			console.log('bb')
			var lastCheckMargin =  slideArea.scrollLeft();
			checkCnt = lastCheckMargin - firstCheckMargin;
			slideArea.find('li').off('touchstart touchmove', swipeRun);
			$('body').css({"overflow" : 'auto'})				
			if(checkCnt < -40) {
				goToslide(currentIndex -1);	
			} else if(checkCnt > 40 ) {					
				goToslide(currentIndex + 1);
			} else {
				goToslide(currentIndex);
			}		
			slideArea.find('li').on({
				'touchstart' : function(ev){
					checkTouchPoint = ev.originalEvent.changedTouches[0].pageX;		
					firstCheckMargin = slideArea.scrollLeft();
					swipeRun(checkTouchPoint, $(this));						
				}
			})			
		}
	})

	var swipeInterval = null;
	function swipeRun(startMove, obj) {
		slideArea.find('li').on({
			'touchmove' : function(ev) {
				if(swipeInterval == null){
					console.log('cc')
					swipeInterval = setTimeout(function(){
						var moveX = ev.originalEvent.changedTouches[0].pageX;
						var listsMargin = slideArea.scrollLeft();
						movePoint = checkTouchPoint - moveX;
						if(movePoint < -20 || movePoint > 20) {
							ev.preventDefault();
							$('body').css({"overflow" : 'hidden'})
							slideArea.scrollLeft(listsMargin + movePoint)
							checkTouchPoint = ev.originalEvent.changedTouches[0].pageX;				
						}
						clearTimeout(swipeInterval)
						swipeInterval = null;
					}, 10)
				}
			}
		})
	}
}

// 06. 메인 유튜브 뉴스 - 범용 세팅(X)
// DNI 서영기
// 전체 Json내부 개수 내 랜덤으로 한가지 뉴스를 sort
function reMainVideoNews() {
    var videoApi = "API/mainVideoEng.json";
    var writeArea = $('#newsMain .newsPlay');

    $.get(videoApi, {}, function(data, xhr){
        var listCount = data.lists.length;
        var randomNumber = Math.floor(Math.random() * listCount);
        var youtubeHtml = 'https://www.youtube.com/embed/'+ data.lists[randomNumber].youtubeLink +'?rel=0';
        writeArea.find('h2').html(data.lists[randomNumber].title)
        writeArea.find('p').html(data.lists[randomNumber].content)
        writeArea.find('iframe').attr('src', youtubeHtml)
    })
}

// 07. 메뉴구조적용
// DNI 서영기
function buildMenuList() {
	var menuAPI = 'API/menuTreeEng.json'
	var alginTarget = new Array;
	$('body > nav').html('<div><span></span></div>')
	var menuSet = $.get(menuAPI,{},function(data){		
		$.each(data.pages, function(){
			stMenuSet(this);
		})
		$('header > div > ul').append('<div></div>');
		$.each(data.pages, function(){
			ndMenuSet(this)
		})
	})

	// st-html세팅
	function stMenuSet(data) {
		var stMenu = ''
		var stMenuLink = '/web/' + lengType + '/';
		//메뉴 html 구성
		if(pageRoot == data.rootDir) {
			if(pageRoot != 'product') {
				$('section > hgroup').css({'background-image' : 'url(/assetMobile/mobile_images/'+data.rootDir+'/bg_title.png)'})
			}
			$('header > h1').append(data.engRootName);
		}
	}

	// nd-depth 메뉴 세팅
	function ndMenuSet (data) {	
		//2depth 메뉴 변수
		var ndRoot = data.rootDir;
		var ndRootLink = '/web/' + lengType + '/';
		if(typeof(data.pageList[0].fileName) != 'undefined'){
			ndRootLink += ndRoot + '/' + data.pageList[0].fileName;
		} else {
			ndRootLink += ndRoot + '/' + data.pageList[0].subMenu[0].fileName;
		}		
		var ndMenu = '<ul>';
		//GNB 메뉴 변수
		var GNBMenu = '<div>';
		GNBMenu += '<h2 onclick="openMenu(this)">' + data.engRootName + '</h2>';
		GNBMenu += '<ul>';
		$.each(data.pageList, function(){
			if(this.engEnbled) {
				var ndFileName = new Array;				
				if(typeof(this.fileName) == 'string'){
					ndFileName = this.fileName.split('/');
					ndFileName = ndFileName[0].split('.');
				}
				var checkLocation = pageLocation.split('/');
				checkLocation = checkLocation[0].split('.')
				var checkPage = false;						
				if(ndFileName[0] == checkLocation[0]) {					
					checkPage = true;
				} else {					
					$.each(this.subMenu, function(){						
						var checkFileName = this.fileName.split('/');
						checkFileName = checkFileName[0].split('.')
						if(checkFileName[0] == checkLocation[0]) {
							checkPage = true;
						}
					})
				}
				var ndMenuLink = '/web/' + lengType + '/';
				
				if(typeof(this.fileName) == 'undefined') {
					var startCnt = -1;
					var endCnt = this.subMenu.length;
					do {
						startCnt ++;									
					} while (!this.subMenu[startCnt].engEnbled)
					ndMenuLink += ndRoot + '/' + this.subMenu[startCnt].fileName;
				} else {
					ndMenuLink += ndRoot + '/' + this.fileName;				
				}
				
				if(pageRoot == ndRoot) {
					if(checkPage) {					
						ndMenu += '<li class="on">\n';
						if(pageRoot != 'product') {
							//개요 예외처리
							$('section > hgroup > h1').text(this.engPageName)							
						}
						if(typeof(this.subMenu) != 'undefined'){
							$('#depth3').html('<select onchange="pageMove(this)"></select>');
							console.log(this.fileName)
							if(typeof(this.fileName) != 'undefined'){														
								$('#depth3 > select').html('<option value="' + ndMenuLink + '" selected="true">Overview</option>');
							}
							rdMenuSet (this, ndRoot);
						}
					} else {					
						ndMenu += '<li>\n';					
					}
					ndMenu += '<a href="'+ ndMenuLink +'">' ;
					ndMenu += this.engPageName ;
					ndMenu += '</a>' ;
					ndMenu += '</li>\n';			
				}			
				//GNB 메뉴 세팅
				if(checkPage) {				
					GNBMenu += '<li class="on">\n';										
				} else {
					GNBMenu += '<li>\n';
				}
				GNBMenu += '<a href="'+ ndMenuLink +'">' ;
				GNBMenu += this.engPageName ;
				GNBMenu += '</a>' ;
				GNBMenu += '</li>\n'
			}
		})
		ndMenu += '</ul>';	
		GNBMenu += '</ul></div>';
		$('body > nav > div > span').append(GNBMenu);
		if(pageRoot == data.rootDir && typeof(data.pageList) != 'undefined') {		
			$('#depth2').html(ndMenu);
			if(data.engMenuAlign == 'mobScroll') {				
				swipingWidth('#depth2', 20);								
			} else if(data.engMenuAlign) {				
				autoWidth($('#depth2 ul'), 20);				
			} else {				
				sameWidthAlign($('#depth2'))
			}
		}
	}

	// rd-depth 메뉴 세팅
	function rdMenuSet (data, root) {		
		if(typeof(data.subMenu) != 'undefined') {
			var pageName = pageLocation.split('/')
			pageName = pageName[0].split('.')
			//$('#depth3').html('<select onchange="pageMove(this)"></select>')
			$.each(data.subMenu, function(){
					if(this.engEnbled) {
					var apiName = this.fileName.split('/');
					apiName = apiName[0].split('.');
					var rdMenuLink = '/web/' + lengType + '/' +  root + '/' + this.fileName;
					var selectWrite = '';
					
					if(pageName[0] == apiName[0]) {
						selectWrite += '<option value="' + rdMenuLink + '" selected="true">'+ this.engPageName +'</option>';					
					} else {
						selectWrite += '<option value="' + rdMenuLink + '">'+ this.engPageName +'</option>';					
					}
					$('#depth3 > select').append(selectWrite);
				}
			})
		}	
	}
}

// 07-1. 메뉴 OPEN 
function openMenu(target) {
	var openArea = $(target).closest('div');
	if(!openArea.hasClass('on')) {
		$('body > nav > div > span > div.on ul').slideUp('fast');
		$('body > nav > div > span > div').removeClass('on');
		openArea.addClass('on');
		openArea.children('ul').slideDown('fast');
	} else {
		openArea.children('ul').slideUp('fast', function(){
			openArea.removeClass('on');	
		})
	}
}

// 07-2. GNB open 
function openGNB() {
	if($('header').hasClass('on')) {
		scrollUnlock();
		$('body').removeClass('swipeLock')
		$('body > nav').animate({'height': 0}, 240, function(){
			$(this).css({'display': 'none'})
			$('header').removeClass('on');
		});
	} else {
		scrollLock();
		$('body').addClass('swipeLock')
		$('header').addClass('on');
		var navHeight = $(window).height() - 50;
		$('body > nav').css({'display' : 'block'});
		$('body > nav').animate({'height': navHeight}, 240);
	}

	function scrollLock(){
	    $('body').on('scroll touchmove mousewheel', function(ev){
	        ev.preventDefault();
	    });
	}
	function scrollUnlock(){
	    $('body').off('scroll touchmove mousewheel');
	}
}