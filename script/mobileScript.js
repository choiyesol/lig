$(document).ready(function(){
	// 01-3. root, page명 체킹
	pagechecking()
	// 01-4. 메뉴구조적용
	buildMenuList()
	if(pageRoot == 'kor' || pageRoot == 'eng' ) {
		mainProductParse();
	}
})

window.onpageshow = function(){
} 


$(window).load(function(){
	// 01. 서브메뉴 존재 식별 - json사용 시 미 적용 예정
	//lowDepthMenuAlign();	
	// 02. 검색폼 자동 너비 맞춤
	autoSearchFormWidth();	
});
$(window).scroll(function(){
	// 03. 2,3depth 메뉴 GNB 바로 하단에 붙이기
	fixedDepthMenu();
})

//01-101. 서브 Nav 자동 width 적용
//최초 none스타일일 시 너비값 계산 후 적용
function autoWidth(target, paddingSize) {	
	var oriWidth = 0;
	var eachWidth = new Array;
	var childCnt = target.children('li').length;						
	var allWidth = $(window).width() - (paddingSize * 2 * childCnt);	
	
	$(target).children('li').each(function() {	
		var thisWidth = $(this).outerWidth()			
		eachWidth.push(thisWidth);
		oriWidth += thisWidth;		
	});
	for (i in eachWidth) {
		console.log(allWidth)
		var fixedWidth = allWidth * (eachWidth[i] / oriWidth)		
		target.children('li').eq(i).css({'width' : fixedWidth + 'px', 'padding-left' : paddingSize + 'px',  'padding-right' : paddingSize + 'px'})		
	}	
}

//01-102. 스와이핑 메뉴 - swipeMenu시 실횅
function swipingWidth(target, paddingSize) {	
	var targetArea = $(target).find('ul');
	var menuCnt = targetArea.children('li').length;
	targetArea.children('li').each(function(){
		$(this).css({
			'padding-left' : paddingSize + 'px',			
			'padding-right' : paddingSize + 'px'
		})
	})
	menuWidth = 0;
	for(var i = 0; i < menuCnt; i++) {		
		menuWidth += Math.ceil(targetArea.children('li').eq(i).outerWidth());		
	}	
	targetArea.css({'width' : menuWidth + 'px'});
	$(target).css({'overflow' : 'auto'});
	
	var selectedScrollLeft = targetArea.find('li.on').offset().left;	
	$(target).scrollLeft(selectedScrollLeft);
}

//01-103. 최초페이지 로드 후 하위 메뉴메뉴 유무 체크 후 실행 (json parse시 미사용)
function sameWidthAlign(target) {		
	var targetMenu = target.children('ul');
	var menuCnt = targetMenu.children('li').length;
	var areaWidth = targetMenu.outerWidth();		
	targetMenu.children('li').width(areaWidth/menuCnt);
}

//01-1. 최초페이지 로드 후 하위 메뉴메뉴 유무 체크 후 실행
function lowDepthMenuAlign() {	
	for (var i= 2 ; i < 3; i ++ ){
		var submenuName = '#depth'+i;
		var menuCheck = $('section').find(submenuName).length;
		if(menuCheck == 1){
			autoWidth($(submenuName).children('ul'), 5);
		}
	}
}

// 01-3. root, page명 체킹
function pagechecking() {	
	var locationArray = document.location.pathname.split('/')
	var checkRootPosition = 0;
	var checkPagePosition = 0;
	if(locationArray.length == 6) {
		checkRootPosition = locationArray.length-3
		checkPagePosition = locationArray.length-2
	} else {
		checkRootPosition = locationArray.length-2
		checkPagePosition = locationArray.length-1
	}
	lengType = locationArray[2]	
	pageRoot = locationArray[checkRootPosition];
	
	var fileNameArray = locationArray[checkPagePosition].split('.');
	pageLocation = fileNameArray[0]	
}

// 01-4. 메뉴구조적용
function buildMenuList() {
	var alginTarget = new Array;
	$('body > nav').html('<div><span></span></div>')
	var menuSet = $.get('/data/menuTree.json',{},function(data){		
		$.each(data.pages, function(){
			stMenuSet(this);			
		})
		$('header > div > ul').append('<div></div>');
		$.each(data.pages, function(){
			ndMenuSet(this)
		})
	})

	// st-html세팅
	function stMenuSet(data) {
		var stMenu = ''
		var stMenuLink = '/web/' + lengType + '/';
		//메뉴 html 구성
		if(pageRoot == data.rootDir) {
			if(pageRoot != 'product') {
				$('section > hgroup').css({'background-image' : 'url(/assetMobile/mobile_images/'+data.rootDir+'/bg_title.png)'})
			}
			$('header > h1').append(data.rootName);
		}

		
	}

	// nd-depth 메뉴 세팅
	function ndMenuSet (data) {	
		//2depth 메뉴 변수
		var ndRoot = data.rootDir;
		var ndRootLink = '/web/' + lengType + '/';
		if(typeof(data.pageList[0].fileName) != 'undefined'){
			ndRootLink += ndRoot + '/' + data.pageList[0].fileName;
		} else {
			ndRootLink += ndRoot + '/' + data.pageList[0].subMenu[0].fileName;
		}		
		var ndMenu = '<ul>';
		//GNB 메뉴 변수
		var GNBMenu = '<div>';
		GNBMenu += '<h2 onclick="openMenu(this)">' + data.rootName + '</h2>';
		GNBMenu += '<ul>';
		$.each(data.pageList, function(){			
			var ndFileName = new Array;				
			if(typeof(this.fileName) == 'string'){
				ndFileName = this.fileName.split('/');
				ndFileName = ndFileName[0].split('.');
			}
			var checkLocation = pageLocation.split('/');
			checkLocation = checkLocation[0].split('.')
			var checkPage = false;						
			if(ndFileName[0] == checkLocation[0]) {					
				checkPage = true;
			} else {					
				$.each(this.subMenu, function(){						
					var checkFileName = this.fileName.split('/');
					checkFileName = checkFileName[0].split('.')
					if(checkFileName[0] == checkLocation[0]) {
						checkPage = true;
					}
				})
			}
			var ndMenuLink = '/web/' + lengType + '/';			
			
			
			if(typeof(this.fileName) == 'undefined') {
				ndMenuLink += ndRoot + '/' + this.subMenu[0].fileName;
			} else {
				ndMenuLink += ndRoot + '/' + this.fileName;				
			}
									
			if(pageRoot == ndRoot) {
				if(checkPage) {
					ndMenu += '<li class="on">\n';				
					
					if(pageRoot != 'product') {
						//개요 예외처리
						if(this.pageName != '개요') {
							$('section > hgroup > h1').text(this.pageName)							
						} else {
							$('section > hgroup > h1').text('투자정보')
						}				
					}
					
					if(typeof(this.subMenu) != 'undefined'){
						$('#depth3').html('<select onchange="pageMove(this)"></select>');
						console.log(this.fileName)
						if(typeof(this.fileName) != 'undefined'){														
							$('#depth3 > select').html('<option value="' + ndMenuLink + '" selected="true">개요</option>');
						}
						rdMenuSet (this, ndRoot);
					}
				} else {					
					ndMenu += '<li>\n';					
				}
				ndMenu += '<a href="'+ ndMenuLink +'">';
				ndMenu += this.pageName ;
				ndMenu += '</a>' ;
				ndMenu += '</li>\n';			
				
			}			
			//GNB 메뉴 세팅
			if(checkPage) {				
				GNBMenu += '<li class="on">\n';
				
			} else {
				GNBMenu += '<li>\n';
			}
			GNBMenu += '<a href="'+ ndMenuLink +'">' ;
			GNBMenu += this.pageName ;
			GNBMenu += '</a>' ;
			GNBMenu += '</li>\n'
			
		})
		ndMenu += '</ul>';	
		GNBMenu += '</ul></div>';
		$('body > nav > div > span').append(GNBMenu);
		if(pageRoot == data.rootDir && typeof(data.pageList) != 'undefined') {		
			$('#depth2').html(ndMenu);
			if(typeof(data.menuAlign) != 'undefined' && data.menuAlign) {				
				autoWidth($('#depth2 ul'), 20);
			} else {
				sameWidthAlign($('#depth2'))
			}
		}
	}

	// rd-depth 메뉴 세팅
	function rdMenuSet (data, root) {		
		if(typeof(data.subMenu) != 'undefined') {
			var pageName = pageLocation.split('/')
			pageName = pageName[0].split('.')
			//$('#depth3').html('<select onchange="pageMove(this)"></select>')
			$.each(data.subMenu, function(){
				var apiName = this.fileName.split('/');
				apiName = apiName[0].split('.');
				var rdMenuLink = '/web/' + lengType + '/' +  root + '/' + this.fileName;
				var selectWrite = '';
				
				if(pageName[0] == apiName[0]) {
					selectWrite += '<option value="' + rdMenuLink + '" selected="true">'+ this.pageName +'</option>';					
				} else {
					selectWrite += '<option value="' + rdMenuLink + '">'+ this.pageName +'</option>';					
				}				
				$('#depth3 > select').append(selectWrite);
			})
		}	
	}
}

// 01-6. 메뉴 OPEN 
function openMenu(target) {
	var openArea = $(target).closest('div');
	if(!openArea.hasClass('on')) {
		$('body > nav > div > span > div.on ul').slideUp('fast');
		$('body > nav > div > span > div').removeClass('on');
		openArea.addClass('on');
		openArea.children('ul').slideDown('fast');
	} else {
		openArea.children('ul').slideUp('fast', function(){
			openArea.removeClass('on');	
		})
	}
}

//02. 검색폼 자동 너비 맞춤
function autoSearchFormWidth() {
	if ($('#searchArea').length > 0) {
		var areaWidth = $('#searchArea').outerWidth();

		var selectWidth = 0;		
		if($('#searchArea > select').length > 0 ) {
			$('#searchArea > select').each(function() {
				selectWidth += $(this).outerWidth() + 10;
			});
		};

		var buttonWidth = 0;
		if($('#searchArea > button').length > 0 ) {
			$('#searchArea > button').each(function() {
				buttonWidth += $(this).outerWidth();
			});
		};
		var inputWidth = areaWidth - selectWidth - buttonWidth		
		$('#searchArea > input[type="text"]').css({'width' : inputWidth+'px'});

		var ulObjCnt = $('#searchArea ul li').length;
		$('#searchArea ul li').css({'width' : areaWidth / ulObjCnt + 'px'})
	}	
}

//03. 2,3depth 메뉴 GNB 바로 하단에 붙이기
function fixedDepthMenu() {
	var windowScroll = $(window).scrollTop();
	var hgroupHeight = $('section > hgroup').outerHeight();
	var GNBHeight = $('header').height();
	if(pageRoot == 'kor' || pageRoot == 'eng') {
		hgroupHeight = $('section:eq(0)').outerHeight();
	}	
	
	if(!$('header').hasClass('on')) {
		if( windowScroll >= (hgroupHeight - GNBHeight)) {
			$('header').css({'background-color' : 'rgba(0,0,0,1)'})
		} else {
			if(pageRoot == 'kor' || pageRoot == 'eng') {
				$('header').css({'background-color' : 'rgba(0,0,0,0)'})
			} else {
				$('header').css({'background-color' : 'rgba(0,0,0,0.3)'})
			}
		}
	}
}

//04. 스크립트 페이지 이동 처리
function pageMove(moveRoot) {
	var moveLink = ''
	if(typeof(moveRoot) == 'object'){
		if(moveRoot.tagName == 'SELECT'){
			$.each($(moveRoot).children('option'), function(){
				if($(this).prop('selected')){
					moveLink = $(this).val();
				};
			})
		} else {
			moveLink = $(moveRoot).val();
		}
	} else {
		moveLink = moveRoot
	}
	console.log(moveLink)
	window.location = moveLink
}

//05. iframe 사이즈 재구성
function iframeResize() {
	var target = $('#disclosureIFrame')
	target.load(function(){
		var oriWidth = 740;
		var oriHeight = 920;
		var screenWidth = $(window).width() - 36;
		sizeLevel = ( screenWidth / oriWidth ).toFixed(3);
		var fixedHeight = 920 * sizeLevel;
		var reverseLevel = -1*(1-sizeLevel)/2;
		console.log(oriHeight * sizeLevel)
		target.css({
			'transform' : 'scale(' + sizeLevel +')',
			'margin-left' : oriWidth * reverseLevel + 'px',
			'margin-top' : oriHeight * reverseLevel + 'px'
		})
		target.parent('div').css({
			'height': (oriHeight * sizeLevel) + 'px'
		})
	})
}

// 06. 투자정보 개요 페이지 주가 숫자 변화
// 출처 - https://codepen.io/uxicode/pen/qzJKRb 
// 일부 스크립팅 변경( parseInt 적용 범위 소수점 구분)
function counterUp( settings ){
    var $settings = settings;
    var $target =$settings.ele;
    var countUpDatas = [];
    var countFuncs;

    var nums = [];
    var delay=$settings.delay || 0.8;
    var time=$settings.time || 160;
    var divisions = time / delay;
    var num = $settings.num;
  //콤마가 있는지 체크 정규식
    var isComma = /[0-9]+,[0-9]+/.test(num);
    num = num.replace(/,/g, '');
  // 숫자 목록 생성
    for (var i = divisions; i >= 1; i--) {
    
    //  int 인 경우 int로 유지
    // var newNum = parseInt(num / divisions * i);
    var nuNum = ''
    if(num == Math.floor(num)) {
        newNum = parseInt(num / divisions * i);
    } else {
        newNum = (num / divisions * i).toFixed(2);
    }
    // console.log( newNum, num / divisions * i)
    // 쉼표가있는 경우 쉼표 유지
    if (isComma) {
        while (/(\d+)(\d{3})/.test(newNum.toString())) {
        newNum = newNum.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
        }
    }
    nums.unshift(newNum);
    }
    countUpDatas=nums;
    $target.text('0');

  // 완료 될 때까지 번호를 업데이트
    function updateNum() {

    $target.text( countUpDatas.shift() );
    
    //숫자를 담고 있는 배열 길이가 존재한다면 계속해서 루프 시킴.
    if ( countUpDatas.length ) {
        setTimeout( countFuncs, delay);
    } else {
    	delete countUpDatas;
    	countUpDatas=null;
    	countFuncs=null;
    }
    // console.log( countUpDatas.length )
    }
    countFuncs=updateNum;
  // 카운트 시작
    setTimeout(countFuncs, delay);
}

// 06-1. 카운트가 여러개일 경우 설정.
// 변수값 커스텀
function numberMotion( targetClass ) {
	var targetData = new Array();
	var targetJson = new Object();

	var targetObj = $('.' + targetClass + ' .changCnt');	

	targetObj.each( function() {
		targetArr = new Array();
		targetArr.num = $(this).attr('data-value');
		targetArr.ele = $(this).index('.changCnt')+1;		
		targetData.push(targetArr);		
	});	

	if(Object.prototype.toString.call( targetData )!=='[object Array]'){ 		
		return 
	}
	for( var i=0;i<targetData.length;i++){
		counterUp( {num:targetData[i].num, ele:$('.changCnt').eq(targetData[i].ele-1) });
	}
}

// 07. GNB open 
function openGNB() {
	if($('header').hasClass('on')) {
		scrollUnlock();
		$('body').removeClass('swipeLock')
		$('body > nav').animate({'height': 0}, 240, function(){
			$(this).css({'display': 'none'})
			$('header').removeClass('on');
		});
	} else {
		scrollLock();
		$('body').addClass('swipeLock')
		$('header').addClass('on');
		var navHeight = $(window).height() - 50;
		$('body > nav').css({'display' : 'block'});
		$('body > nav').animate({'height': navHeight}, 240);
	}

	function scrollLock(){
	    $('body').on('scroll touchmove mousewheel', function(ev){
	        ev.preventDefault();
	    });
	}
	function scrollUnlock(){
	    $('body').off('scroll touchmove mousewheel');
	}
}

// 08. 좌우 슬라이드 메뉴 type1
// type1은 자동 너비 조절이 되는 목록 show hide형식
function slideMenu (target, viewMenuLength) {	
	var Cnt = 0;
	var parentWrap = $('.'+target);	
	var maxslideCnt = parentWrap.children('div').children('ul').children('li').length - viewMenuLength;
	var menuObj = parentWrap.children('div').children('ul').children('li');
	var menuObjWidth = parentWrap.children('div').outerWidth() / viewMenuLength;
	parentWrap.children('div').children('ul').children('li').width(menuObjWidth)
	parentWrap.children('div').children('ul').width(menuObj.length * menuObjWidth)

	$.each(menuObj, function(){
		if($(this).css('display') == 'none') {
			Cnt++;
		}
	})
	
	menuDisabled(Cnt)

	parentWrap.children('button').on('click', function(){
		var menuEq = $(this).index();		
		if(menuEq == 0) {
			Cnt--;
			menuObj.eq(Cnt).css({'display':'block', 'width':'1px'})
			menuObj.eq(Cnt).animate({'width': menuObjWidth + 'px'}, 200);			
		} else {
			Cnt++;			
			menuObj.eq(Cnt-1).animate({'width': 0}, 200, function() {
					$(this).css({'display':'none'})
			});
		}
		menuDisabled(Cnt)
	})
	
	function menuDisabled(cntValue) {		
		if(cntValue == 0) {
			parentWrap.children('button').eq(0).prop('disabled', true);
			if (menuObj.length <= viewMenuLength){
				parentWrap.children('button').eq(1).prop('disabled', true);	
			}
		} else if(cntValue == maxslideCnt) {
			parentWrap.children('button').eq(1).prop('disabled', true);
		} else {
			parentWrap.children('button').prop('disabled', false);
		}
	}
	
}

// 09. 좌우 슬라이드 메뉴 type2
// type2는 내부 리스트 요소가 좌우로 이동되는 형식
// 타겟 > nav + div > ul > li로 구성되어야 한다.
// 반드시 규칙적 마진이 두번째 자식요소에 부여되어야 한다.
function slideMove (target, minimumCnt, movePosition) {	
	var divWidth = $('.'+ target).children('div').width();
	var listArea = $('.'+ target).children('div').children('ul');
	var listCnt = listArea.children('li').length;
	var listwidth = 1;
	var listMargin = 0;
	var listeachWidth = 0;
	listArea.children('li').each(function(){
		listwidth += Number($(this).css('margin-right').replace('px',''));
		listwidth += $(this).outerWidth()
		listwidth += Number($(this).css('margin-left').replace('px',''));
	})
	if(listArea.children('li').length == 1) {
		listeachWidth = listArea.children('li').eq(0).outerWidth();
	} else {
		listeachWidth = listArea.children('li').eq(1).outerWidth();
		if (typeof(listArea.children('li').eq(1).css('margin-left')) != 'undefined' && listArea.children('li').eq(1).css('margin-left') != 0) {
			listMargin = Number(listArea.children('li').eq(1).css('margin-left').replace('px',''));
		}
	}	
	var moveWidth = listeachWidth + listMargin;
	

	if(typeof(movePosition) == 'undefined') {		
		listAreaTotalWidth = (listwidth + ( listMargin * (listCnt - 1))) + 1;
		listArea.width(listAreaTotalWidth)		
		if(listCnt < minimumCnt) { 
			$('.'+ target).children('button').css('display','none')
		}
		//인디케이터 생성
		//ol.indicator의 존재여부로 li를 생성한다.
		if($('.'+ target).find('ol.indicator').length == 1) {			
			for(i=0; i<listCnt; i++ ) {			
				$('.'+ target).find('ol.indicator').append('<li></li>');
			}
			$('.'+ target).find('ol.indicator').children('li').eq(0).addClass('on')
			$('.'+ target).find('ol.indicator').children('li').on({
				'mouseup' : function(){
					var targetIndex = $(this).index();
					var runMove = targetIndex * moveWidth;
					$('.'+ target).find('ol.indicator').children('li').removeClass('on');					
					$(this).addClass('on');			
					slideMove (target, minimumCnt, runMove)
				}
			})
		}
		bindAction();

		//버튼 동작
		function moveAction(buttonIndex){		
			$('.'+ target).children('button').unbind('click')			
			var maxPostion = -1* (listArea.outerWidth() - divWidth);			
			var nextPostion = Math.floor( maxPostion / moveWidth)+1
			var checkCnt = 0;
			var nowPosition = Number(listArea.css('margin-left').replace('px',''));	
			if($('.'+ target).find('ol.indicator').length == 1) {				
				$('.'+ target).find('ol.indicator').children('li').each(function(){
					if($(this).hasClass('on')){
						checkCnt = $(this).index();
					}
				});
			}
			if(buttonIndex == 0) {

				$('.'+ target).find('ol.indicator').children('li').removeClass('on')
				if(checkCnt - 1 < 0) {
					checkCnt = checkCnt
				} else {
					checkCnt -- 
				}
				$('.'+ target).find('ol.indicator').children('li').eq(checkCnt).addClass('on')

				if(nowPosition == 0) {
					listArea.animate({'margin-left': 65},
					120, function() {
							listArea.animate({'margin-left': 0},60,function(){							
								// 홍보센터 사보영역 위한 액션			
								if (pageRoot == 'prcenter' && pageLocation == 'newsletter'){
									newsLetterListAct(0);
								}
								// 액션 재 바인딩
								bindAction();
							})
					});
				} else if(nowPosition == maxPostion) {				
					listArea.animate({'margin-left': nextPostion * moveWidth},
						80, function() {						
							// 홍보센터 사보영역 위한 액션			
							if (pageRoot == 'prcenter' && pageLocation == 'newsletter'){
								newsLetterListAct(nextPostion * moveWidth);
							}
							// 액션 재 바인딩
							bindAction();
					});
				} else {
					listArea.animate({'margin-left': nowPosition + moveWidth},
						180, function() {						
							// 홍보센터 사보영역 위한 액션			
							if (pageRoot == 'prcenter' && pageLocation == 'newsletter'){
								newsLetterListAct(nowPosition + moveWidth);
							}
							// 액션 재 바인딩
							bindAction();
					});	
				}
			} else {

				$('.'+ target).find('ol.indicator').children('li').removeClass('on')
				if(checkCnt + 1 == listCnt) {
					checkCnt = checkCnt;
				} else {
					checkCnt ++ 
				}
				$('.'+ target).find('ol.indicator').children('li').eq(checkCnt).addClass('on');				
				if(nowPosition == maxPostion) {					
					listArea.animate({'margin-left': maxPostion - 65 },
						120, function() {
							listArea.animate({'margin-left': maxPostion},60,function(){							
								// 홍보센터 사보영역 위한 액션			
								if (pageRoot == 'prcenter' && pageLocation == 'newsletter'){
									newsLetterListAct(maxPostion);
								}
								// 액션 재 바인딩
								bindAction();
							})						
					});
				} else if ((nowPosition - moveWidth) < maxPostion) {					
					listArea.animate({'margin-left': maxPostion},
						80, function() {						
							// 홍보센터 사보영역 위한 액션			
							if (pageRoot == 'prcenter' && pageLocation == 'newsletter'){
								newsLetterListAct(maxPostion);
							}
							// 액션 재 바인딩
							bindAction();
					});	
				} else {					
					listArea.animate({'margin-left': nowPosition - moveWidth},
						180, function() {						
							// 홍보센터 사보영역 위한 액션			
							if (pageRoot == 'prcenter' && pageLocation == 'newsletter'){
								newsLetterListAct(nowPosition - moveWidth);
							}
							// 액션 재 바인딩
							bindAction();
					});	
				}
			}		
		};
		function bindAction() {		
			$('.'+ target).children('button').bind('click', function(){
				var buttonIndex = $(this).index();		
				moveAction(buttonIndex)			
			})
		}
	} else {
		movePosition = -1 * movePosition
		listArea.animate({'margin-left': movePosition},
			180, function() {						
				// 홍보센터 사보영역 위한 액션			
				if (pageRoot == 'prcenter' && pageLocation == 'newsletter'){
					newsLetterListAct(nowPosition - moveWidth);
				}				
		});	
	}	
}

autoSlideTimeSet = '';
function autoSlide(target, timeoutDelay, infinite) {
	var targetArea = $('.'+target).children('div').children('ul');
	var areaWidth = $('.'+target).children('div').outerWidth();
	var listWidth = targetArea.children('li').outerWidth();	
	var listMargin = 0;
	if(targetArea.children('li').length != 1) {
		listMargin = Number(targetArea.children('li').eq(1).css('margin-left').replace('px',''))
	}
	var maxMargin = targetArea.outerWidth() - areaWidth;

	rollingAct();
	targetArea.on({
		'touchstart' : function(ev) {
			stopAct();
		},
		'touchend' : function(ev) {
			rollingAct();
		}
	})

	function rollingAct() {	
		autoSlideTimeSet = setInterval(function(){
			var nowMargin = Number(targetArea.css('margin-left').replaceAll('px',''));
			var nextMove = nowMargin - (listWidth + listMargin);
			if(nextMove < -1 * maxMargin && (typeof(infinite) == 'undefined' || !infinite)) {
				nextMove = 0;
			}
			if ($('.'+target).find('ol.indicator').length == 1) {
				var nowIndex = $('.'+target).find('ol.indicator').find('li.on').index();				
				$('.'+target).find('ol.indicator').children('li').removeClass();
				if((nowIndex + 1) == targetArea.children('li').length) {
					nowIndex = 0;
				} else {
					nowIndex += 1;
				}
				$('.'+target).find('ol.indicator').children('li').eq(nowIndex).addClass('on')
			}
			targetArea.animate({ 'margin-left' : nextMove}, 240);
		}, timeoutDelay)
	}

	function stopAct() {
		clearInterval(autoSlideTimeSet)
		autoSlideTimeSet = null
	}

}

// 09-1. 좌우 슬라이드 메뉴 type3
// 슬라이드 2 선언 후 스와이프 액션 선언
swipeTimeOut = null;
function swipeAct(actArea) {
	var listsObj = $('.' + actArea).children('div').children('ul');
	var listsWidth = listsObj.children('li').eq(1).outerWidth();	
	var firstCheckMargin = 0;
	var checkTouchPoint = 0;
	var movePoint = 0;
	listsObj.find('li').children('div').on({
		'touchstart' : function(ev){			
			checkTouchPoint = ev.originalEvent.changedTouches[0].pageX;
			firstCheckMargin = Number(listsObj.css('margin-left').replace('px',''))
			swipeRun(checkTouchPoint, $(this));
		},
		'touchend' : function(ev){			
			var lastCheckMargin =  Number(listsObj.css('margin-left').replace('px',''))
			checkCnt = lastCheckMargin - firstCheckMargin;
			listsObj.find('li').children('div').off('touchstart touchmove', swipeRun);
			$('body').css({"overflow" : 'auto'})
			var newCheckIndex = listsObj.find('li.on');
			if(checkCnt < -30 && !newCheckIndex.next('li').hasClass('blank')) {				
				moveAct(newCheckIndex.next('li'), 'slideNewLetterList')					
			} else if(checkCnt > 30 && !newCheckIndex.prev('li').hasClass('blank')) {				
				moveAct(newCheckIndex.prev('li'), 'slideNewLetterList')					
			} else {
				listsObj.animate({"margin-left": lastCheckMargin - checkCnt}, 30)
			}		
			listsObj.find('li').children('div').on({
				'touchstart' : function(ev){
					checkTouchPoint = ev.originalEvent.changedTouches[0].pageX;		
					firstCheckMargin = Number(listsObj.css('margin-left').replace('px',''))	
					swipeRun(checkTouchPoint, $(this));
				}				
			})
	}})

	function swipeRun(startMove, obj) {
		listsObj.find('li').children('div').on({
			'touchmove' : function(ev) {
				var moveX = ev.originalEvent.changedTouches[0].pageX;
				var listsMargin = Number(listsObj.css('margin-left').replace('px',''));				
				movePoint = checkTouchPoint - moveX;
				if(movePoint < -20 || movePoint > 20) {
					ev.preventDefault();
					$('body').css({"overflow" : 'hidden'})		
					listsObj.css({"margin-left" : (listsMargin - movePoint) + 'px'});				
					checkTouchPoint = ev.originalEvent.changedTouches[0].pageX;				
				}
			}
		})
	}
}

// 10. 홍보센터 > 사보관련 액션
// 13-1. 사보 최초 로드 시 실행
function newsLetterAct(areaName) {
	var slideArea = $('.'+areaName).children('div').children('ul')
	slideArea.prepend('<li class="blank"></li>');
	slideArea.append('<li class="blank"></li>');

	slideArea.children('li').each(function(){	
		var thisHeight = (($(this).children('div').outerWidth() /4)*5);		
		$(this).children('div').height(thisHeight);
		$(this).children('div').children('div').height(thisHeight);
	})	
	$('.newsLetter article > ul li').each(function(){		
		var thisHeight = (($(this).children('div').outerWidth() /4)*5);		
		$(this).children('div').children('div').height(thisHeight)
	})
	newsLetterListAct(0)
	slideMove ('slideNewLetterList', 3)
	swipeAct('slideNewLetterList')
}
// 13-2. 슬라이드 동작 완료 후 bind 완료 후 동작
function newsLetterListAct(marginNumber) {	
	var selectLists = $('.slideNewLetterList').children('div').children('ul').children('li');
	selectLists.removeClass('on')
	selectLists.children('div').children('div').attr('style','');
	var listsWidth = selectLists.eq(1).width();
	var listsMargin = Number(selectLists.eq(1).css('margin-left').replace('px',''));
	var cnt =  -1*(marginNumber / (listsWidth + listsMargin));
	selectLists.eq(cnt+1).addClass('on');
	selectLists.eq(cnt+1).on('mouseenter', function(){
		$(this).children('div').children('div').attr('style','');
	});
	
	var targetSeq = selectLists.eq(cnt+1).attr('data-seq')
	var targetTitle = selectLists.eq(cnt+1).attr('data-title')
	var SNSFBLink = selectLists.eq(cnt+1).attr('data-snsFB')
	var SNSBlogLink = selectLists.eq(cnt+1).attr('data-snsBlog')
	var fileDownloadLink = selectLists.eq(cnt+1).attr('data-pdfLink')
	// 확인 후 실행
	var webzinLink = selectLists.eq(cnt+1).attr('data-webzinLink')

	$('.slideNewLetterList > p').children('button').each(function() {		
		var runFunction = '';
		if($(this).hasClass('snsFB')){
			// 페이스북 버튼 제어
			if($('#insideIPYn').val() != "Y"){
				runFunction = "fnFBShare('" + targetSeq + "', '" + targetTitle + "')"
			} else {
				runFunction = "alert('내부망에서는 사용하실 수 없습니다.')"
			}
			$(this).attr('onclick', runFunction)
		} else if ($(this).hasClass('snsBlog')){
			// 블로그 버튼 제어
			if($('#insideIPYn').val() != "Y"){
				runFunction = "fnBlogShare('" + targetSeq + "', '" + targetTitle + "')"
			} else {
				runFunction = "alert('내부망에서는 사용하실 수 없습니다.')"
			}
			$(this).attr('onclick', runFunction)
		} else if ($(this).hasClass('download')){
			// 파일 다운로드 버튼 제어
			runFunction = "downloadFileIdx('prData', '"+targetSeq+"', '"+fileDownloadLink+"')";
			// runFunction = "window.open('" + fileDownloadLink + "')"
			$(this).attr('onclick', runFunction)
		} 
		 else if ($(this).hasClass('goTo')){
		 	// 웹진링크 제어
			 if($('#insideIPYn').val() != "Y"){
				runFunction = "window.open('" + webzinLink + "')"
			} else {
				runFunction = "alert('내부망에서는 사용하실 수 없습니다.')"
			}
			$(this).attr('onclick', runFunction)
		}
	});
}
// 13-3. 선택 게시글 이동
function moveAct(obj, target, slideCheck) {
	var moveArea = $('.'+target).children('div').children('ul')
	var nowMargin = moveArea.css('margin-left');
	var targetSeq = $(obj).attr('data-seq');
	var targetIndex = 0;
	moveArea.children('li').each(function() {
		if (targetSeq == $(this).attr('data-seq')) {
			targetIndex = $(this).index();
		}
	});
	var listsWidth = moveArea.find('li.on').width();
	var listsMargin = Number(moveArea.find('li.on').css('margin-left').replace('px',''));
	var moveMargin = (targetIndex-1) * (-1*(listsWidth + listsMargin))

	if(!$(obj).hasClass('on')){
		if(typeof(slideCheck) != 'undefined') {
			var movetop = $('#'+slideCheck).offset().top - $('body > header').height();		
			 $('html, body').animate({'scrollTop': movetop}, 240, function(){
			 	gotoSelectPostion();
			 });
		} else {
			gotoSelectPostion();
		}
	} else {
		viewNewsletterSummury('slideNewLetterList')
	}
	

	function gotoSelectPostion() {
		moveArea.animate({'margin-left': moveMargin}, 240, function() {
			newsLetterListAct(moveMargin)
		});
	}	
}
// 13-4. 뉴스레터 미리보기
function viewNewsletterSummury(target){		
	$('.'+target).children('div').children('ul').children('li').each(function(){
		if($(this).hasClass('on')){
			var viewDiv = $(this).children('div').children('div')
			if(viewDiv.css('opacity') == 0) {
				var checkHeight = viewDiv.parent('div').outerHeight();
				console.log(checkHeight)
				viewDiv.css({'display': 'block', 'height' : checkHeight + 'px'});
				viewDiv.animate({'opacity': 1},80, function(ev){
					$('.'+target).children('div').children('ul').children('li').children('div').off();
				});				
			} else {
				viewDiv.animate({'opacity': 0},80, function(){					
					viewDiv.css({'display': 'none'});
					viewDiv.attr('style','');
					$('.'+target).children('div').children('ul').children('li').children('div').off()
					swipeAct(target);
				});
			}
		}
	});
}

// 14. 스와이프 관련
// 14-1. noneStop 스와이프 : 자동 맞춤 없음
function swipeMove (target, autoStop) {
	var actArea = $('.'+target).children('div').children('ul');
	var setMargin = 0;
	if (typeof(actArea.css('margin-left')) != 'undefined' && actArea.css('margin-left') != 0) {
		setMargin = Number(actArea.css('margin-left').replace('px', ''));
	}
	var listObj = actArea.children('li')
	var listWidth = actArea.find('li').outerWidth();
	var totalListLength = actArea.find('li').length;
	var listMargin = 0
	if(totalListLength != 0 && typeof(actArea.find('li').eq(2).css('margin-left')) != 'undefined') {
		listMargin = Number(actArea.find('li').eq(2).css('margin-left').replace('px', ''));
	}
	var areaTotalWidth = 0;
	actArea.children('li').each(function(){		
		var fixedWidth = Math.ceil($(this)[0].getBoundingClientRect().width);
		//console.log(fixedWidth);
		$(this).outerWidth(fixedWidth);
		areaTotalWidth += Number($(this).css('margin-right').replace('px',''));
		areaTotalWidth += fixedWidth;
		areaTotalWidth += Number($(this).css('margin-left').replace('px',''));
	})
	areaTotalWidth += setMargin;
		
	//actArea.width(areaTotalWidth + totalListLength);
	actArea.width(areaTotalWidth);
	
	if(areaTotalWidth > $(window).width()){
		swipeStart();
	}
	var checkTouchPoint = 0;
	var startMargin = 0;
	function swipeStart() {
		listObj.on({
			'touchStart' : function(ev){
				checkTouchPoint = ev.originalEvent.changedTouches[0].pageX;
				swipeRun(checkTouchPoint);				
			},
			'touchend' : function(ev) {
				if(autoStop) {
					swipeAutoEnd();
				}
				listObj.off('touchstart touchmove', swipeRun);
				$('body').css({"overflow" : 'auto'})
				listObj.on({
					'touchstart' : function(ev){
						checkTouchPoint = ev.originalEvent.changedTouches[0].pageX;
						swipeRun(checkTouchPoint);
					}
				})
			}
		})
	}
	
	function swipeRun(startMove) {
		startMargin = Number(actArea.css("margin-left").replace('px',''))
		listObj.on({
			'touchmove' : function(ev) {
				var moveX = ev.originalEvent.changedTouches[0].pageX;
				var listsMargin = Number(actArea.css('margin-left').replace('px',''));
				movePoint = checkTouchPoint - moveX;				
				if(movePoint < -20 || movePoint > 20 ) {
					$('body').css({"overflow" : 'hidden'})
					if((listsMargin - movePoint) < setMargin && (listsMargin - movePoint) > -1 * (areaTotalWidth - $(window).width())) {
						actArea.css({"margin-left" : (listsMargin - movePoint) + 'px'});				
						checkTouchPoint = ev.originalEvent.changedTouches[0].pageX;						
					} else {
						return;
					}					
				}
			}
		})
	}
	function swipeAutoEnd(checkTouchPoint) {
		var nowMargin = Number(actArea.css("margin-left").replace('px',''))		
		if(startMargin > nowMargin && (startMargin - nowMargin) > 30 && startMargin >  -1 * (areaTotalWidth - $(window).width())) {			
			var moveMargin = -1*(startMargin - listWidth);			
			if( moveMargin < -1 * (areaTotalWidth - $(window).width())) {
				moveMargin = -1 * (areaTotalWidth - $(window).width())
			}
			slideMove (target, 1, moveMargin)			
			if($('.'+target).find('ol.indicator').length == 1) {				
				var nowIndex = $('.'+target).find('ol.indicator').find('li.on').index();
				console.log(nowIndex)
				$('.'+target).find('ol.indicator').children('li').removeClass();
				$('.'+target).find('ol.indicator').children('li').eq(nowIndex + 1).addClass('on')
			}
		} else if(startMargin < nowMargin && (startMargin - nowMargin) < -30 && startMargin < 0) {
			var moveMargin = -1 * (startMargin + listWidth)			
			slideMove (target, 1,moveMargin)
			if($('.'+target).find('ol.indicator').length == 1) {
				var nowIndex = $('.'+target).find('ol.indicator').find('li.on').index();				
				$('.'+target).find('ol.indicator').children('li').removeClass();
				$('.'+target).find('ol.indicator').children('li').eq(nowIndex - 1).addClass('on')
			}
		} else {			
			slideMove (target, 1, -1*startMargin) 
		}
	}
}


// 15. 홍보센터 > 영상 유튜브 재생관련
function movieChange(act) {
	var preAddress = 'https://www.youtube.com/embed/'
	var movAddress = $(act).attr('data-videoClipURL')
	var targetIframe = $(act).closest('article').children('iframe')
	var targetList = $(act).closest('ul').children('li');
	targetList.removeClass('on')
	targetIframe.attr('src', preAddress + movAddress + '?rel=0');
	$(act).addClass('on')
}

// 16. 지속가능경영
// 16-1. 윤리준법 - 4depth메뉴 동작
function viewArea(obj, targetPreName) {
	var indexCnt = $(obj).index() + 1;
	var allCnt = $(obj).parent('ul').children('li').length;
	console.log(allCnt)
	$(obj).parent('ul').children('li').removeClass('on');
	$(obj).addClass('on');
	for(var i= 0; i < allCnt ; i ++){		
		$('.' + targetPreName + (i+1)).css({'display' : 'none'});	
	}
	$('.' + targetPreName + indexCnt).css({'display' : 'block'});
}

// 16-2. 신고센터 내용 Open
function openArea (obj) {
	var lists = $(obj).parent('ul');
	var minheight = $(obj).children('h1').outerHeight();
	var openHeight = $(obj).children('div').outerHeight();
	if(!$(obj).hasClass('on')) {				
		$(obj).animate({'height': (openHeight + minheight)}, 300, function() {
			$(this).addClass('on');
		});		
	} else {
		$(obj).animate({'height': minheight}, 200, function() {
			$(this).removeClass('on');
		});
	}	
}

// 16-3. 현재 input의 id값과 일치하는 class의 div 표시/숨김
function viewDiv (obj) {	
	var targetClassName = $(obj).attr('id');
	//console.log(targetClassName)
	$(obj).closest('td').children('div').css({'display':'none'})
	$('.'+targetClassName).css({'display':'block'})
}

// 17. 메인페이지 액션
// 17-1. 메인 사업영역 Json Parse - 개발 커밋시 수정
function mainProductParse() {	
	$.get('../../data/productTree.json', {}, function(data){		
		var menuWrite = '';
		var bgTree = '';
		$.each(data.pages, function(){
			menuWrite += '<li>\n';
			menuWrite += '<a href="/web/'+ lengType +'/product/product.do?category=' + this.categoryKey +'">';
			menuWrite += this.categoryName;
			menuWrite += '</a>';
			menuWrite += '<div style="background-image: url(\'/assetMobile/mobile_images/product/bg_categorySelect_'+ this.categoryKey +'.png\')"></div>'
			menuWrite += '</li>\n';
			bgTree += '<div style="background-image: url(\'/assetMobile/mobile_images/product/bg_main'+ this.categoryKey +'.png\')"></div>'
		})
		$('#mainProduct ul').html(menuWrite);
		$('#mainProduct').append(bgTree);
		$('#mainProduct ul li').on({
			'touchstart' : function(){
				$('#mainProduct ul li').removeClass('on');
				$('#mainProduct > div').removeClass('on');
				var divIndex = $(this).index();
				$(this).addClass('on');
				$('#mainProduct > div').eq(divIndex).addClass('on');
			},
			'touchend touchmove' : function() {
				$('#mainProduct ul li').removeClass('on');
				$('#mainProduct > div').removeClass('on');
			}
		})
		$('#mainRecruit ul li, #mainEtcMenu ul li, #mainNews div ul li').on({
			'touchstart' : function(){
				$(this).parent('ul').children('li').removeClass('on');				
				$(this).addClass('on');
			},
			'touchend touchmove' : function() {
				$(this).parent('ul').children('li').removeClass('on');				
			}
		})
	})
}
// 17-2. 메인 인디케이터 정렬
function fullIndicatorAct(target) {
	var targetArea = $('.' + target).find('ul');
	var listLength = targetArea.children('li').length;	
	var listWidth = (100/listLength) + '%';	
	$('.' + target).find('ol.indicator').children('li').width(listWidth)
}

// 18. 인재채용 
// 18-1. 복리후생 관련 - 아이콘 자동 배치
function iconBatch(obj){
	var tableCnt = 1;	
	$(obj).find('table').each(function(){		
		var tdCnt = 1;;
		$(this).find('td').each(function(){			
			var imgAddr = '/assetMobile/mobile_images/recruit/icon_benefits_'
			imgAddr += tableCnt;
			if(tdCnt < 10) {
				imgAddr += '0'
			}
			imgAddr += tdCnt;
			imgAddr += '.png';					
			$(this).css({'background-image': 'url(\'' + imgAddr + '\')'})
			tdCnt ++;
		})
		tableCnt ++
	})
}
// 18-2. 복리후생 관련 -  ahrfhr 
function changeView(obj, target) {
	var chkCnt = $(obj).index();
	$(obj).parent('ol').children('li').removeClass('on')
	$(obj).addClass('on')
	$(target).find('table').removeClass('on')
	$(target).find('table').eq(chkCnt).addClass('on')
}

//19 무한 롤링
setInfinityMove = null;
function infinitySlideMove (target, setDelay) {
	var targetArea = $('.' + target).children('div').children('ul');
	var startMargin = Number($('.' + target).children('div').children('ul').css('margin-left').replace('px',''));	
	var moveButton = $('.' + target).children('button');
	var areaWidth = 0;	
	targetArea.children('li').each(function() {
		areaWidth += Number($(this).css('margin-left').replace('px',''));
		areaWidth += $(this).outerWidth();
		areaWidth += Number($(this).css('margin-right').replace('px',''));
	});
	targetArea.css({'width' : areaWidth})
	targetArea.children('li').first().addClass('on')
	targetArea.prepend(targetArea.children('li').last().clone());
	targetArea.children('li').last().remove();

	var listMargin = 0;
	if(targetArea.children('li').length != 1) {
		listMargin = Number(targetArea.children('li').eq(1).css('margin-left').replace('px',''))
	}
	var listTotalMargin = $('.' + target).children('div').children('ul').children('li').eq(1).outerWidth() + listMargin

	buttonAct();
	
	if(typeof(setDelay) != 'undefined') {
		setIntervalMove();
		targetArea.on({
			'mouseenter' : function(){
				clearInterval(setInfinityMove);
				setInfinityMove = null;
			},
			'mouseleave' : function(){
				setIntervalMove();
			}
		})	
	}
	function buttonAct() {
		moveButton.on({
			'click' : function(){
				
				if($(this).index() == 0) {
					preMove()
				} else {
					nextMove()
				}
			},
			'mouseenter' : function(){
				if(typeof(setDelay) != 'undefined') {
					clearInterval(setInfinityMove);
					setInfinityMove = null;
				}
			},
			'mouseleave' : function(){
				if(typeof(setDelay) != 'undefined') {
					setIntervalMove();
				}
			},
		})
	}
	function preMove() {
		moveButton.off();
		var moveMargin = startMargin - listTotalMargin;			
		targetArea.animate({ 'margin-left' : moveMargin}, 240, function(){
			buttonAct()
			targetArea.css({'margin-left' : startMargin + 'px'})			
			targetArea.append(targetArea.children('li').first().clone());	
			targetArea.children('li').first().remove();
			targetArea.css({'width' : areaWidth + 'px'})
			targetArea.children('li').removeClass();
			targetArea.children('li').eq(1).addClass('on');
		});	
	}
	function nextMove() {
		moveButton.off();
		var moveMargin = startMargin - listTotalMargin;		
		targetArea.css({'margin-left' : moveMargin + 'px'})
		targetArea.prepend(targetArea.children('li').last().clone());		
		targetArea.children('li').last().remove();
		targetArea.animate({ 'margin-left' : startMargin}, 240, function(){
			buttonAct()						
			targetArea.children('li').removeClass();
			targetArea.children('li').eq(1).addClass('on');
		});
	}

	function setIntervalMove(){
		setInfinityMove = setInterval(nextMove, setDelay);
	}
}

//19-3. 직무소개 Json 관련
function parseIntroduction(obj, viewCnt) {
	var targetArea = $(obj).children('ul');	
	var listCnt = 1;
	var writeList = '';
	var viewCnts = 	viewCnt ? viewCnt : 4;
	var startCnt = viewCnts - 3
	$.get('/data/introduction.json', {}, function(data){
		$.each(data.lists, function(){	
			var listCntString = listCnt;
			if(listCntString < 10){
				listCntString = '0' + listCntString
			}
			if(listCnt >= startCnt && listCnt <= viewCnts) {
				writeList += '<li style="background-image: url(\'/assetMobile/mobile_images/recruit/photo_introduction_'+ listCntString +'_01.png\')"';
				writeList += 'onclick="top.location.href=\'view.do?interviewID=' + listCnt + '\'"';
				writeList += '>\n';
				writeList += '<h1>' + this.part + '</h1>\n';
				writeList += this.team + '<br>\n';
				writeList += this.name + '&nbsp;' + this.position + '\n'
				writeList += '</li>\n'			
			}
			listCnt ++
		})
		targetArea.append(writeList)
		$(obj).find('button').remove();
		if(viewCnts < (listCnt -1)) {
			var wrtieButton = '<button type="button" onclick="parseIntroduction(\''+ obj +'\',' + (viewCnts + 4) + ')">';
			wrtieButton += '더보기 (' + viewCnts + '/' + (listCnt - 1) + ') +';
			wrtieButton += '</button>';
			$(obj).append(wrtieButton);
		}
	});
}

// 20. Main sideNav
function movePart(target) {
	var movetop = 0
	var headerHeight = $('header').outerHeight() + 30;
	if(typeof(target) != 'undefined') {
		movetop = $(target).offset().top - headerHeight;
	}
	$('html, body').animate({'scrollTop': movetop}, 140);
}

// 21. Main slide 재구성
var mainSlidingSetTime = null;
function mainImgMove() {
	var target = $('#mainSlide');
	var listArea = target.children('div').children('ul')
	var listLength = listArea.children('li').length	
	listArea.children('li').each(function(){
		var dataSeq = $(this).attr('data-seq');
		var listWrite = '<li data-seq="'+ dataSeq +'"></li>';
		target.children('ol').append(listWrite);
	})	
	target.children('ol').children('li').css({'width': (100 / listLength) + '%'})
	target.children('ol').children('li').first().addClass('on')
	var aeraWidth = $(window).width() * listLength;
	if(listLength == 1) {
		$(target).children('button').css({'display': 'none'});
	} else {
		if(listLength == 2) {
			aeraWidth = $(window).width() * 4;
			var copyHtml = listArea.html();
			listArea.append(copyHtml);
		}
		var lastList = listArea.children('li').last();
		listArea.prepend(lastList.clone());
		lastList.remove();
		listArea.css({'margin-left': -1 * $(window).width() + 'px'});
	}	
	listArea.css({'width' : aeraWidth + 'px'})
	
	buttonBind();
	setRollingInterval();
	bindSwipe();
	//버튼 클릭 시 이동
	function unbindButton(){
		target.children('button').off();
	}
	function buttonBind(){
		target.children('button').on({
			'click' : function(){
				clearRollingInterval();				
				if($(this).index() == 0){
					slideMove();
				} else {
					slideMove(true);
				}
				setRollingInterval();				
			}
		})
	}
	// 스와이프 세팅
	var startX = 0;
	function bindSwipe() {
		listArea.children('li').on({
			'touchstart' : function(ev){							
				startX = ev.originalEvent.changedTouches[0].pageX;				
				swipeAct(startX);
			},
			'touchend' : function(ev) {				
				var endCheck = ev.originalEvent.changedTouches[0].pageX;				
				swipeEnd (endCheck);				
			}
		});
	}	
	function unbindSwipe() {		
		listArea.children('li').off();
	}
	
	function swipeAct(startPoint) {
		var startPointCheck = startPoint;		
		listArea.children('li').on({
			'touchmove' : function(ev){				
				unbindButton();
				clearRollingInterval();
				var movePoint = ev.originalEvent.changedTouches[0].pageX
				var moveMargin = startPointCheck - movePoint;
				var nowMargin = Number(listArea.css('margin-left').replace('px',''))
				var swipeCheck = 0;
				if(startX < movePoint) {
					swipeCheck = movePoint - startX ;
				} else {
					swipeCheck = startX - movePoint;
				}				
				if(swipeCheck > 30) {
					$('body').css({'overflow' : 'hidden'});					
					listArea.css({'margin-left': (nowMargin - moveMargin) + 'px' })
					startPointCheck = movePoint;
				}
			}
		})
	}
	
	function swipeEnd(endPoint) {
		$('body').css({'overflow' : 'auto'});	
		buttonBind()		
		setRollingInterval()
		if(startX - endPoint < -20) {
			slideMove()
		} else if(startX - endPoint > 20) {
			slideMove(true)
		} else {
			listArea.animate({'margin-left' : -1 * $(window).width()}, 100);
		}		
	}
	
	//인터벌 롤링 관련
	function clearRollingInterval() {
		clearInterval(mainSlidingSetTime);
		mainSlidingSetTime = null;
	}
	function setRollingInterval(){
		mainSlidingSetTime = setInterval(function(){
			unbindButton();			
			slideMove(true);
			buttonBind();			
		}, 8000)
	}
	
	// 슬라이드 정의 next = boolean
	function slideMove(next) {
		var cloneList;
		var onIndicatorSeq = 0;
		unbindSwipe()
		if(!next) {
			listArea.animate({'margin-left' : 0}, 400, function(){
				cloneList = listArea.children('li').last();
				listArea.prepend(cloneList.clone());
				cloneList.remove();
				listArea.css({'margin-left' : (-1 * $(window).width()) + 'px'});
				onIndicatorSeq = listArea.children('li').eq(1).attr('data-seq');	
				indicatorAct(onIndicatorSeq);
				bindSwipe();
			})
		} else {
			listArea.animate({'margin-left' : -2* $(window).width() + 'px'}, 400, function(){
				cloneList = listArea.children('li').first();
				listArea.append(cloneList.clone());
				cloneList.remove();
				listArea.css({'margin-left' : (-1 * $(window).width()) + 'px'});
				onIndicatorSeq = listArea.children('li').eq(1).attr('data-seq');
				indicatorAct(onIndicatorSeq);
				bindSwipe();
			})
		}
	}
	// 인디케이터 이동
	function indicatorAct(selIndex) {		
		target.children('ol').children('li').removeClass('on')
		target.children('ol').children('li').each(function(){
			if($(this).attr('data-seq') == selIndex){
				$(this).addClass('on')
			}
		})
	}
	
}

//21. 메인 모달 닫기
function modalClose(obj) {
	$(obj).parent('div').parent('div').css({'display' : 'none'})
}

//99. 내용물 byte 체크
function printBytes(target) {
	$(target).each(function(){
		var checkChar = $(this).html();				
		console.log(getBytes(checkChar));
	})
}
function getBytes(str) {
	var cnt = 0;
	for(var i=0; i<str.length;i++) {
		cnt += (str.charCodeAt(i) > 128) ? 2: 1;
	}
	return cnt;
}


//99-2. 파라미터 가져오기
//출처: https://handam.tistory.com/41 [우리는 오랜만에 편히 앉아서 한담을 나누었다]
$.urlParam = function(name){
  var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
  if (results==null){
     return null;
  }
  else{
     return results[1] || 0;
  }
}

//99-3. 에디터 내용 내 태그 삭제
//출처: https://webisfree.com
//태그변경 : </p>의경우 줄바꿈 유지
function removeTag(target) {
	var oriHtml = target.html()
	var removeRun = oriHtml.replace(/\n/gi, "").replace(/&nbsp;/gi, "").replace(/<br>/gi, "").replace(/<\/p>/gi, "&brSpace;").replace(/(<([^>]+)>)/ig,"").replace(/&brSpace;&brSpace;/gi, '&brSpace;').replace(/&brSpace;/gi, '\n');
	removeRun = removeRun.trim();
	//var removeRun = removeArea.replace(/(<|<\/)p([\s.'"=a-zA-Z]*)>/gi, "");
	target.html(removeRun);
}