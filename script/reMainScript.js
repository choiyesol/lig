//접근성용 포커스링 관련 세팅
$(window).on({
	'keyup' : function(e){
		var nowFoucs = $('body').find('.inTabOutline');
		var focusElement = document.activeElement;		
		if(e.keyCode == '9' || e.keyCode == '13' || e.keyCode == '32' || e.keyCode == '37' || e.keyCode == '38' || e.keyCode == '39' || e.keyCode == '40' || e.keyCode == '16' && e.keyCode == '9' || e.keyCode == '16' ){
			nowFoucs.removeClass('inTabOutline');
			if(!$(focusElement).hasClass('inTabOutline')){
				$(focusElement).addClass('inTabOutline')
			}			
		}
	},
	'click' : function(){
		var nowFoucs = $('body').find('.inTabOutline');
		nowFoucs.removeClass('inTabOutline');
	},
	'blur' : function(){
		var nowFoucs = $('body').find('.inTabOutline');
		nowFoucs.removeClass('inTabOutline');
	}
})


$(function(){

    //#businessMain 멀티슬라이드 -DNI 최예솔
    moveBusinessSlide() ;

    //topBtn 상단 이동 -DNI 최예솔
    moveTop();

    //하단슬라이드 -DNI 최예솔
    bottomSlide ();

	// 초기 루트 세팅 -DNI 서영기
	pagechecking()

	// 메인 GNB 세팅 -DNI 서영기
	buildMenuList();

    // 메인 슬라이드 -DNI 서영기
    reMainSlideAct();

    // 메인 LIG소식 소팅 -DNI 서영기
    reMainVideoNews();

	// select 재구성 -DNI 서영기
	selectRemakAll();

	// 마우스휠 상단 영역 컨트롤
	autoScroll();

	// 영역 스크롤 이벤트
	scrollEvent();
}); 

// 01. #businessMain 멀티슬라이드
// DNI 최예솔
function moveBusinessSlide(){
	var productAPI = 'API/productTree.json';
    var busislideArea = $('#businessMain > div');

	var ajaxAction = $.get(productAPI, {}, function(data){
		var listHtml = '';
		$.each(data.pages, function(){
			var bgName = 'images/reMain/img_main' + this.categoryKey
			listHtml += '<li style="background-image: url(\''+ bgName +'.png\')">\n';			
			listHtml += '<a href="\'/web/' + lengType + '/product/product.do?category=' + this.categoryKey +'\'">\n';
            listHtml += '<div>\n<div>\n';
            listHtml += '<div class="imgArea"><img src="images/reMain/ico_main'+ this.categoryKey +'.png" alt="'+ this.categoryName +' 아이콘"></div>\n';
            listHtml += '<h2>'+ this.categoryName +'</h2>\n';
            listHtml += '<span>View more</span>\n';
			listHtml += '</div>\n</div>\n';
            listHtml += '</a>\n';
            listHtml += '</li>\n';
		})
		busislideArea.find('ul').html(listHtml);
		resizeSlideArea();
	})
	
	ajaxAction.done(function(){
		buttonBind(true);
		// 버튼 액션 영역 시작		
		//direction = prev, next, undefined
		function moveOption(direction){
			var maxScroll = busislideArea.children('ul').outerWidth() - $(window).outerWidth();
			var movebusiSlide = $('#businessMain li').width();		
			console.log(movebusiSlide)
			var nowScroll = busislideArea.scrollLeft();
			console.log('스크롤값'+ movebusiSlide)
			buttonBind()
			console.log(maxScroll)
			if(direction == 'next') {
				if(nowScroll == maxScroll) {
					busislideArea.animate({'margin-left' : '-20px'}, 180, function(){
						busislideArea.animate({'margin-left' : 0}, 60)
						buttonBind(true)
					})					
				} else {
					busislideArea.animate({'scrollLeft' : nowScroll + movebusiSlide}, 240, function(){
						buttonBind(true)
					})
				}
				// 무한 슬라이드 백업용
				// busislideArea.animate({'scrollLeft' : movebusiSlide}, 240, function(){
				// 	busislidTarget = busislideArea.find('li').first();
				// 	busislidTarget.clone().appendTo(busislideArea.children('ul'))
				// 	busislidTarget.remove();
				// 	busislideArea.scrollLeft(0);
				// 	buttonBind(true)				
			} else {
				if(busislideArea.scrollLeft() == 0) {
					busislideArea.animate({'margin-left' : '20px'}, 180, function(){
						busislideArea.animate({'margin-left' : 0}, 60)
						buttonBind(true)
					})					
				} else {
					busislideArea.animate({'scrollLeft' : nowScroll - movebusiSlide}, 240, function(){
						buttonBind(true)
					})					
				}
				// 무한슬라이드 백업용
				// busislidTarget =  busislideArea.find('li').last();
				// busislidTarget.clone().prependTo(busislideArea.children('ul'))
				// busislidTarget.remove();
				// busislideArea.scrollLeft(movebusiSlide)
				// busislideArea.animate({'scrollLeft' : 0}, 240)
				// buttonBind(true)
			}
		}

		//binded = boolean
		function buttonBind(binded){
			if(binded == true) {
				$('#businessMain > button').on({
					'click' : function(){
						if($(this).hasClass('busibtnPre')){
							moveOption('prev')
						} else {
							moveOption('next')
						}
					}
				})
			} else {
				$('#businessMain > button').off('click')
			}
		}
	})

	// resize 체크 1366 미만일 시 동작 하지 않음
	var resizeInterval = null;
	$(window).on({		
		'resize' : function(){
			if($(window).outerWidth() > 1366 && resizeInterval == null){
				resizeInterval = setTimeout(function(){					
					// resizeInterval(nowindex)
					clearTimeout(resizeInterval)
					resizeSlideArea()
					busislideArea.scrollLeft(0); 
					resizeInterval = null;
				},50)				
			}
		}
	})	
	
	function resizeSlideArea() {	
		var listCnt = busislideArea.find('li').length;
		var listWidth = $(window).outerWidth()/4
		$('#businessMain li').width(listWidth);
		console.log(listWidth)
		busislideArea.children('ul').css({'width': listCnt * listWidth + 'px'})
		// busislideArea.scrollLeft(0); 
	}	
}

// 02. topBtn 상단 이동
// DNI 최예솔
function moveTop (){
    $('.topBtnArea').click(function(){
        $('html, body').animate({scrollTop :0 },700);
    });
}

// 03. reMain 하단슬라이드
// DNI 최예솔
function bottomSlide (){
    var slidecounter = $('#slideMain > div ul li').length;
    var slideWidth = $('#slideMain > div ul li').outerWidth();
    var currentIndex = 0;

    $('#slideMain > div ul').css({'width' : slideWidth * slidecounter + 'px' });
    // $('#slideMain > div ul').css('margin-left');

    creatIndicator();
    clickSlideBtn();
    clickPagingBtn();

    //03-1 SlidIndicator 생성
    function creatIndicator (){
        if(slidecounter >= 2){
            var slideIndicator ='';
            $('#slideMain > div ul li').each (function(i){
                slideIndicator += '<li';
                if(i==0) {
                    slideIndicator += ' class="btnPaging on"';
                } else {
                    slideIndicator += ' class="btnPaging"';
                }
                slideIndicator += '><a href="javascript:;">' + ( i +1) + '번째 이미지</a>\n</li>'
            });
            $('#slideMain > .slidIndicator').html(slideIndicator);
        }
    }

    //03-2  SlidIndicator ON
    function goToslide(index){
		var nowScroll = $('#slideMain > div').scrollLeft();
		console.log(index * - slideWidth)
        // $('#slideMain > div').animate({'scrollLeft': (index * - slideWidth)}, 420);
        $('#slideMain > div ul').animate({marginLeft: (index * - slideWidth) + 'px'}, 420);
        currentIndex = index;
        $('#slideMain .slidIndicator').children('li').removeClass('on');
        $('#slideMain .slidIndicator').children('li').eq(currentIndex).addClass('on');
    }

    //03-3 paging 버튼 click
    function clickPagingBtn (){
        $('#slideMain .slidIndicator').children('.btnPaging').click(function(){
            //stopTimer()
            var idx = $(this).index();
            goToslide(idx);
            //slideTimer()
        });
    }

    // 03-4 좌우 버튼 (prev, next) click
    function clickSlideBtn(){
        $('#slideMain > a').click(function(e){
            //stopTimer()
            e.preventDefault();
            if($(this).hasClass('btnPrev')){
                if(currentIndex == 0){ 
                    $('#slideMain > div ul').animate({paddingLeft:'40px', opacity:0.3 },240,function(){
                        $(this).animate({paddingLeft: '0',opacity:1 },180);
                    });
                } else {
                    goToslide(currentIndex -1);
                }
            } else{
                if(currentIndex == (slidecounter-1)){
                    $('#slideMain > div ul li').animate({marginRight:'-25px',opacity:0.3},240,function(){
                        $(this).animate({marginRight: '0',opacity:1 },180);
                    });
                    console.log(currentIndex);
                } else{
                    goToslide(currentIndex + 1);    
                }
            }
            //slideTimer()
        });
    }
    //03-5 자동슬라이드
    var timer = null;
    function slideTimer(){
        if(timer == null){
            timer = setInterval(function(){
            var nextIndex = (currentIndex + 1)%slidecounter;
            goToslide(nextIndex)
        },3000);
        }
    }
    //slideTimer()
    function stopTimer(){
        clearInterval(timer)
        timer = null;
    }
	//접근성 focusin 
	//DNI 최예솔
	function btnFocusin (){
		$('#slideMain > div > ul > li  a').on({
			'focusin':function(){
				var nowIndex = $(this).closest('li').index();				
				goToslide(nowIndex)
				$('#slideMain > div').scrollLeft(0);				
				$('#slideMain .slidIndicator').children('li').removeClass('on');
				$('#slideMain .slidIndicator').children('li').eq(nowIndex).addClass('on');
			}
		}) 
	}
	btnFocusin();
}

// 04. root, page명 체킹
// DNI 서영기
// 최초 페이지 로드시 현재 루트를 및 언어 설정을 확인한다.
var lengType = '';
function pagechecking() {	
	var locationArray = document.location.pathname.split('/')
	var checkRootPosition = 0;
	var checkPagePosition = 0;
	if(locationArray.length == 6) {
		checkRootPosition = locationArray.length-3
		checkPagePosition = locationArray.length-2
	} else {
		checkRootPosition = locationArray.length-2
		checkPagePosition = locationArray.length-1
	}
	lengType = locationArray[2]	
	pageRoot = locationArray[checkRootPosition];
	
	var fileNameArray = locationArray[checkPagePosition].split('.');
	pageLocation = fileNameArray[0]	
}

// 05. 메인 슬라이드 - 범용 세팅(X)
// DNI 서영기
// 기본 슬라이드 세팅 li기준 자동 인디케이터 및 width 생성
function reMainSlideAct(){
    var reMainSlideTime = 10000;
    var reMainSlideSpeed = 240;
    var reMainSlideInterval = null;
    var reMainSlideAct = true;

    // 초기 슬라이드 영역 세팅
    // 기본 슬라이드와 별개의 동작 세팅
    var moveArea = $('.mainSlideArea > div');
    var indicatorArea = $('.mainSlideArea > ol.indicator');
	moveArea.children('ul').css({'width' : (moveArea.find('li').length * 100) + 'vw'});
    moveArea.find('li').each(function(i){
        var indexNumber = i;
        if(indexNumber < 10) {
            indexNumber = '0'+ (i+1)
        }

        var dataSeq = $(this).attr('data-seq');
        var indicatorList = '<li data-seq="' + dataSeq + '">\n';                
        indicatorList += '<a href="javascript:;">' + indexNumber + '<div><span></span></div></a>\n';
        indicatorList += '</li>\n';
        indicatorArea.append(indicatorList);
    })
	
	// 슬라이드 영역 버튼 width정리
	// indicatorArea 기본 leftmargin = margin-left: calc(50% - 661px);
	indicatorArea.find('li').first().addClass('on')
	$('.mainSlideArea > button:eq(1)').css({'margin-left': 'calc(50% - '+ (661 - indicatorArea.outerWidth()) +'px)'})
	$('.mainSlideArea > button.stopPlay').css({'margin-left': 'calc(50% - '+ (661 - 41 - indicatorArea.outerWidth()) +'px)'})
    intervalMainSlide();
    reMainIndicatorAct();
    reMainSlideBind();

    function intervalMainSlide() {
        if(reMainSlideInterval == null) {
            reMainSlideInterval = setInterval(function(){
                moveMainSlide('next');                    
            }, reMainSlideTime)
        }
    }

    function stopMainSlide() {
        clearInterval(reMainSlideInterval);
        reMainSlideInterval = null;
    }

    //unbinded = boolean 
    //바인드 여부를 결정한다.
    function reMainSlideBind(unbinded) {
        if(!unbinded) {
            $('.mainSlideArea > button:not(".stopPlay")').on({
                'click': function(){
                    stopMainSlide()
                    if($(this).index() == 1) {
                        moveMainSlide('prev');
                    } else if($(this).index() == 2) {
                        moveMainSlide('next');
                    }
                    if(reMainSlideAct) {
                        intervalMainSlide()
                    }
                }
            })
			$('.mainSlideArea > button.stopPlay').on({                
                'click': function(){
                    if($(this).hasClass('on')){
						reMainSlideAct = true;
						$(this).removeClass('on')
						intervalMainSlide();
						reMainIndicatorAct();
						$(this).html('재생하기')
					}else {
						reMainSlideAct = false;
						$(this).addClass('on')
						stopMainSlide();
						$(this).html('멈춤')
					}
				}
            })
			$('.mainSlideArea > button.nextArea').on({                
                'click': function(){
					var moveArea = $('#businessMain').offset().top - 90;					
                    $('body, html').animate({'scrollTop': moveArea}, 180)
				}
            })
            indicatorArea.find('li').on({
                'click': function(){
                    stopMainSlide()
                    moveMainSlide(Number($(this).attr('data-seq')));
                    if(reMainSlideAct) {
                        intervalMainSlide()
                    }
                }
            })			
        } else {
            $('.mainSlideArea > button').off('click')
            indicatorArea.find('li').off('click')
			// $('.mainSlideArea > div > ul > li button').off('click')
        }
    }	

    // stopCheck = boolean
	// 접근성으로 인한 focusin에 대한 바인드 취소 후 재 바인딩
    function reMainIndicatorAct(stopCheck) {
		focusinAction() 
        var checkSeq = $('.mainSlideArea > div li').eq(0).attr('data-seq');
        var checkIndex = 0;
        indicatorArea.children('li').each(function(i){
            if($(this).attr('data-seq') == checkSeq){
                checkIndex = $(this).index();
            }
        })            
        indicatorArea.children('li').find('span').clearQueue(false);            
        indicatorArea.children('li').removeClass('on');
        indicatorArea.children('li').find('span').css({'width' : '0'});
        indicatorArea.children('li').eq(checkIndex).addClass('on');
        if(stopCheck){
            indicatorArea.children('li').eq(checkIndex).find('span').css({'width' : '100%'})
        } else {
            indicatorArea.children('li').eq(checkIndex).find('span').animate({'width' : '100%'}, reMainSlideTime)
        }
		focusinAction(true)
    }

    function moveMainSlide(dataSeq) {
        stopMainSlide();
        reMainSlideBind(true);
        var moveLeftWidth = $(window).width();
        var afterFunction = '';
        var appended = 1;
        if(typeof(dataSeq) == 'number') {
            moveArea.find('li').each(function(i){                    
                if($(this).attr('data-seq') == dataSeq) {
                    appended = i;
                }
            })
        } else if(dataSeq == 'prev') {                 
            appended = -1;                
        }
        
        var moveTarget = '';
        if(appended == -1) {
            moveArea.scrollLeft(moveLeftWidth)
            moveTarget = moveArea.find('li').last();
            moveTarget.clone().prependTo(moveArea.children('ul'))
            moveTarget.remove();
            moveArea.animate({'scrollLeft' : 0}, reMainSlideSpeed, function(){
                reMainIndicatorAct()
            })
        } else if( appended > 0 ) {                        
            moveArea.animate({'scrollLeft' : appended * moveLeftWidth}, reMainSlideSpeed, function(){                    
                for(var i= 0; i < appended; i++) {                        
                    moveTarget = moveArea.find('li').first();
                    moveTarget.clone().appendTo(moveArea.children('ul'))
                    moveTarget.remove();
                    moveArea.scrollLeft(0);
                }
                reMainIndicatorAct();
            })                
        } else {                
            reMainIndicatorAct();
        }
        if(reMainSlideAct) {
            intervalMainSlide()
        }
        reMainSlideBind();
    }
	// 접근성 focusin 
	// 서영기
	// binded = boolean
	function focusinAction(binded){
		if(binded) {
			$('.mainSlideArea > div > ul > li  button').on({
				'focusin':function(){
					stopMainSlide();					
					var nowIndex = Number($(this).closest('li').index());
					var listWidth = $(this).closest('li').outerWidth();					
					$('.mainSlideArea > div').scrollLeft(0);
					$('.mainSlideArea > div').scrollLeft(nowIndex * listWidth);
				}
			}) 
		} else {			
			$('.mainSlideArea > div > ul > li  button').off('focusin')
		}
	}
}

// 06. 메인 유튜브 뉴스 - 범용 세팅(X)
// DNI 서영기
// 전체 Json내부 개수 내 랜덤으로 한가지 뉴스를 sort
function reMainVideoNews() {
    var videoApi = "API/mainVideo.json";
    var writeArea = $('#newsMain .newsPlay');

    $.get(videoApi, {}, function(data, xhr){
        var listCount = data.lists.length;
        var randomNumber = Math.floor(Math.random() * listCount);
        var youtubeHtml = 'https://www.youtube.com/embed/'+ data.lists[randomNumber].youtubeLink +'?rel=0';
        writeArea.find('h2').html(data.lists[randomNumber].title)
        writeArea.find('p').html(data.lists[randomNumber].content)
        writeArea.find('iframe').attr('src', youtubeHtml)
    })
}

// 07. 메뉴구조 Json 적용
function buildMenuList() {
    var menuApi = 'API/menuTree.json';
	var alginTarget = new Array;
	var menuSet = $.get(menuApi,{},function(data){		
		stMenuSet(data);			
		ndMenuSet(data);		
	})

	// st-html세팅
	function stMenuSet(data) {
		$.each(data.pages, function(){
			var stMenu = ''
			var stMenuLink = '/web/' + lengType + '/';
			// 메뉴 html 구성
			if(pageRoot == this.rootDir) {
				stMenu += '<li class="on">\n';
				$('#pageTitle').css({'background-image' : 'url(/asset/images/'+this.rootDir+'/bg_title.png)'})				
			}else{				
				stMenu += '<li>\n';
			}		
			// 링크 적용
			if(typeof(this.pageList) == 'undefined') {				
				stMenuLink += this.rootDir + '/' + this.fileName;
			} else {
				if(typeof(this.pageList[0].fileName) != 'undefined' ) {
					stMenuLink += this.rootDir + '/' + this.pageList[0].fileName;
				} else {
					stMenuLink += this.rootDir + '/' + this.pageList[0].subMenu[0].fileName;
				}			
			}
			stMenu += '<a href="'+ stMenuLink + '"'
			if(pageRoot == this.rootDir) {
				stMenu += ' title="현재 선택된 대 메뉴"';
				if(pageRoot != 'product'){
					$('head > title').html('LIG 넥스원 ' + this.rootName)
				}
			}
			stMenu += '>' + this.rootName + '</a>\n';		
			stMenu += '</li>\n';
			$('header > div > ul').append(stMenu);	
		})
	}

	// nd-depth 메뉴 세팅
	function ndMenuSet (data) {
		$.each(data.pages, function(){
			var ndRoot = this.rootDir;			
			// 2depth 메뉴 변수
			var GNBMenu = '<ul>'
			$.each(this.pageList, function(){				
				var ndFileName = new Array;				
				if(typeof(this.fileName) == 'string'){
					ndFileName = this.fileName.split('/');
					ndFileName = ndFileName[0].split('.');
				}
				var checkLocation = pageLocation.split('/');
				checkLocation = checkLocation[0].split('.')
				var checkPage = false;						
				if(ndFileName[0] == checkLocation[0]) {					
					checkPage = true;
				} else {
					if(typeof(this.subMenu) != 'undefined'){
						$.each(this.subMenu, function(){
							var checkFileName = this.fileName.split('/');
							checkFileName = checkFileName[0].split('.')
							if(checkFileName[0] == checkLocation[0]) {
								checkPage = true;
							}
						})
					}
				}
				var ndMenuLink = '/web/' + lengType + '/';
				
                if(typeof(this.fileName) == 'undefined') {
                    ndMenuLink += ndRoot + '/' + this.subMenu[0].fileName;
                } else {
                    ndMenuLink += ndRoot + '/' + this.fileName;
                }
                // GNB 메뉴 세팅
                if(checkPage) {				
                    GNBMenu += '<li class="on">\n';										
                } else {
                    GNBMenu += '<li>\n';
                }
                GNBMenu += '<a href="'+ ndMenuLink +'">' ;
                GNBMenu += this.pageName ;
                GNBMenu += '</a>' ;
                GNBMenu += '</li>\n'				
			})			
			GNBMenu += '</ul>';            
			$('body > header > div > div').append(GNBMenu);
			//$('body > header > div').addClass('lock');			
		})		
		setTimeout(GNBAct, 10);
	}
}

// 08. select요소 (select 재구성)
// DNI 서영기
// 기존 script동일
function selectRemaker(target) {
	var objID =  target.attr('id');
	var selectDisabled = target.prop('disabled');	
	var selectOption = target.children('option:selected').text();
	var selectOptionVal = target.children('option:selected').val();
	var selectTitle = target.attr('title') ? target.attr('title') : '검색어'
	var direction = target.attr('data-direction');
	
	$(target).wrap('<div class="selectRemake"></div>')
	var newSelect = '';		
	if(!selectDisabled) {		
		newSelect += '<a href="javascript:openList(\''+ objID +'\', \''+ direction +'\');" class="selectRemaker" title="' + selectTitle + ' 옵션선택" data-target="' + objID + '" data-value="' + selectOptionVal + '">' + selectOption + '</a>\n';			
	} else {
		newSelect += '<a href="javascript:;" class="disabled">' + selectOption + '</a>\n';
	}
	target.after(newSelect);	
	var selectWidth = target.width();
	target.parent('div').children('a').width(selectWidth);
}

function openList(targetID, direction) {	
	var listWrite = '';
	var targetSelect = $('#' + targetID);
	var nowSelectOption = targetSelect.children('option:selected').val();
	var writeArea = targetSelect.parent('div');
	// console.log(writeArea.find('ul').length)
	if(writeArea.find('ul').length == 0){
		$('.selectRemake').each(function(){
			$(this).children('ul').remove();
		})
		listWrite += '<ul>\n';
		targetSelect.children('option').each(function(i){
			listWrite += '<li>\n';
			listWrite += '<a href="javascript:;" onclick="changeRemakeList(\''+ targetID +'\', \''+  $(this).val() +'\');" data-value="'+ $(this).val() +'">'+ $(this).html() +'</a>\n';
			listWrite += '</li>\n';
		});
		listWrite += '</ul>\n';		
		writeArea.append(listWrite)
		// 펼침 방향에 따른 option 위치 조정
		if(direction == 'up'){		
			var optionHeight = writeArea.children('ul').outerHeight();
			console.log(optionHeight)
			// var selectHeight = writeArea.children('input').outerHeight();
			writeArea.children('ul').css({'top' : ((-1*(optionHeight))+1) + 'px'})
		}
		writeArea.find('ul a').each(function(i){			
			if($(this).attr('data-value') == nowSelectOption){
				$(this).focus();
			}
		})
		writeArea.find('ul a').on({
			'keydown' : function(e){				
				e.preventDefault();
				e.stopPropagation();
				var maxLength = $(this).closest('ul').children('li').length;
				var nowIndex = $(this).parent('li').index();
				console.log(maxLength, nowIndex, e.keyCode)
				//방향키 위, 왼쪽 : 이전목록
				//방향키 아래, 오른쪽 : 다음목록
				//탭, ESC : 셀렉트 목록 삭제
				//엔터 : 선택, 실행
				if(e.keyCode == 37 || e.keyCode == 38) {
					if(nowIndex != 0) {
						$(this).closest('ul').children('li').eq(nowIndex - 1).children('a').focus();
					}					
				} else if(e.keyCode == 39 || e.keyCode == 40) {										
					if(nowIndex != maxLength - 1) {
						$(this).closest('ul').children('li').eq(nowIndex + 1).children('a').focus();
					}
					
				} else if(e.keyCode == 27 || e.keyCode == 9) {
					writeArea.find('ul').remove();
					writeArea.find('a').focus();
				} else if(e.keyCode == 13) {
					$(this).click();
				}
				return false;
			}
		})
	} else {
		writeArea.find('ul').remove();
	}		
}

function changeRemakeList(targetID, value) {	
	var targetSelect = $('#' + targetID).parent('div');	
	targetSelect.find('select').val(value).prop('selected',true);
	targetSelect.children('a').html(targetSelect.find('option:selected').text());
	targetSelect.children('a').attr('data-value', targetSelect.find('option:selected').val())
	if( typeof($('#' + targetID).attr('onchange')) != 'undefined' || $('#' + targetID).attr('onchange') != '' ){
		var runfunction = $('#' + targetID).attr('onchange');
		//eval 대체 함수
		(new Function('return ' + runfunction))();
	}	
	targetSelect.children('ul').remove();
}




// 08-1 select요소 전체 재구성
function selectRemakAll() {
	$('select').each(function() {
		var directionCheck  = $(this).attr('data-direction');		
		selectRemaker($(this), directionCheck);		
	});
	
	$(document).on({
		'click' : function(){
			$('.selectRemake').each(function(){
				$(this).children('ul').remove();
			})
		}
	})
}

function pageMove(targetID) {	
	var locationURL = $('#'+targetID).find('option:selected').val();
	if(confirm('새창으로 해당페이지를 여시겠습니까?')){
		window.open(locationURL)
	}
}

// 19. GNB 전체 메뉴 오픈
function GNBAct() {		
	var openHeight = 0;	
	bindGNBOpenAction();

	// 웹 접근성 용 GNB 제어
	focusAct();
	
	function bindGNBOpenAction() {
		$('body > header > div').on({
			mouseover : function(){
				if(!$('body > header > div').hasClass('lock')) {
					GNBOpenAction();
				} else {
					$('body > header > div').removeClass('lock')
				}
			},
			mouseleave : function(){
				if(!$('body > header > div').hasClass('lock')) {
					GNBCloseAction();
				} else {
					$('body > header > div').removeClass('lock')
				}				
			}
		});
		$('section > hgroup, article').on({
			mouseenter: function() {				
				$('body > header > div').removeClass('lock')
			}
		});
		$('body').on({
			mouseleave : function(e){				
				GNBCloseAction();
			}
		})
	}

	function GNBOpenAction(){		
		if(!$('body > header').hasClass('on')){
			var openHeight = $('body > header > div > div').outerHeight() + 90;			
			$('body > header > a').children('img').css({'margin-top':'-41px'});			
			$('body > header').addClass('on');
			$('body > header > div').animate({'height': openHeight}, 80)
		}
	}

	function GNBCloseAction(){
		if($('body > header').hasClass('on')){			
			var closeHeight = 90;			
			$('body > header > div').animate({'height': closeHeight}, 80, function() {			
				$('body > header > a').children('img').css({'margin-top':'0'});
				$('body > header').removeClass('on');
			});	
			
		}
	}
	
	// 접근성 관련 포커스 액션
	// IE 탭이동 오류 수정
	var userAgent = window.navigator.userAgent;	
	var isIE11 = userAgent.indexOf('Trident');
	var isIEUnder11 = userAgent.indexOf('MSIE');
	var IEcheckMenuArr = new Array();

	function focusAct() {
		$('body > header > div a').on({
			'focus' : function(){
				GNBOpenAction();
				if(pageRoot == 'kor'){
					$('body > header > div a').attr('tabindex', '0')
				}	
			},
			'blur' : function(){
				setTimeout(function(){
					if($('body > header > div').find('a:focus').length == 0){
						GNBCloseAction();
						if(pageRoot == 'kor'){
							$('body > header > div a').attr('tabindex', '-1')
						}						
					}
				}, 500)
			}
			
		})
		var prevMove = false;		
		var closeMenuCheck = false;
		var GNBIndexCnt = 0;
		$('body > header > div > ul > li > a').on({
			'keyup' : function(e){
				if(e.shiftKey && e.keyCode == 9) {
					prevMove = true;
				} else {
					prevMove = false;
				}
			},
			'focus' : function(e){				
				GNBIndexCnt = $(this).parent('li').index();
				if(isIE11 == '-1' || isIEUnder11 == '-1') {
					IEcheckMenuArr.push(GNBIndexCnt)
				}				
			},
			'blur' : function(e){				
				var maxCnt = $(this).closest('ul').children('li').length;
				var subMenuTarget = $('body > header > div > div > ul');				
				if(isIE11 == '-1' && isIEUnder11 == '-1') {
					if(!prevMove){						
						subMenuTarget.eq(GNBIndexCnt).children('li').eq(0).children('a').focus();
					} else if(prevMove){
						console.log(GNBIndexCnt)
						if(GNBIndexCnt != 0) {
							subMenuTarget.eq(GNBIndexCnt-1).find('li:last-child').children('a').focus();
						}
					}
				} else {
					if(!prevMove){						
						subMenuTarget.eq(IEcheckMenuArr[0]).children('li').eq(0).children('a').focus();
						if(IEcheckMenuArr.length == 2) {
							IEcheckMenuArr = [];
						}
					} else if(prevMove){						
						setTimeout(function(){							
							if(IEcheckMenuArr[0] == (maxCnt-1) && IEcheckMenuArr[1] == 0) {
								$('body > header > div > ul > li:first-child() > a').focus();
								IEcheckMenuArr = [];						
							} else {								
								subMenuTarget.eq(IEcheckMenuArr[1]).children('li').last().children('a').focus();
								if(IEcheckMenuArr.length >= 2) {
									IEcheckMenuArr = [];
								}		
							}							
						},30)
					}		
				}				
			}
		})

		var subMenuPrevMove = false;
		var firstSubmenuCheck = false;
		var lastSubmenuCheck = false;
		$('body > header > div > div > ul a').on({
			'keyup' : function(e){
				if(e.shiftKey && e.keyCode == 9) {
					subMenuPrevMove = true;
				} else if(e.keyCode == 9) {
					subMenuPrevMove = false;
				}
			},
			'focus' : function(){
				var maxCnt = $(this).closest('ul').children('li').length;
				var indexCnt = $(this).parent('li').index();
				var SubMenuLenth = $('body > header > div > div > ul').length;
				var SubMenuIndex = $(this).closest('ul').index();				
				if((maxCnt -1) == indexCnt) {
					lastSubmenuCheck = true;
				} else if(indexCnt == 0) {
					firstSubmenuCheck = true;
				} else {
					lastSubmenuCheck = false;
					firstSubmenuCheck = false;					
				}
				if(SubMenuLenth == (SubMenuIndex + 1) && (maxCnt -1) == indexCnt) {
					closeMenuCheck = true;
				} else {
					closeMenuCheck = false;
				}
			},
			'blur' : function(){				
				var GNBCnt = $(this).parent('li').parent('ul').index();
				if(lastSubmenuCheck && !subMenuPrevMove) {					
					$('body > header > div > ul > li').eq(GNBCnt + 1).children('a').focus();
					lastSubmenuCheck = false;
				} else {					
					lastSubmenuCheck = false;
				}
				if(firstSubmenuCheck && subMenuPrevMove) {					
					$('body > header > div > ul > li').eq(GNBCnt).children('a').focus();
					firstSubmenuCheck = false;
				} else {					
					firstSubmenuCheck = false;
				}
				if(closeMenuCheck) {					
					if(isIE11 != '-1' || isIEUnder11 != '-1') {
						IEcheckMenuArr = new Array();
					}
				}
			}		
		})
	}
}
// 10. 접근성 관련 GNB 상단 컨텐츠 바로가기 버튼 액션
// DNI 서영기
function contentFocus(){
	$('#contentsStart').focus();
}

// 11. 메인슬라이드 영역, 메인 사업영역 마우스 휠 제어
// DNI 서영기
function autoScroll(){
	bindWheel(true)
	function bindWheel(useBoolean) {
		if(useBoolean) {			
			$("#introMain, #businessMain").on('wheel DOMMouseScroll', function(e) {
				e.preventDefault();
				var evnetListen = e.originalEvent;				
				if (evnetListen.deltaY < 0) {
					autoScroll('up', this);
				}else{			
					autoScroll('down', this);
				};				
			});
		} else {
			$("#introMain, #businessMain").off('wheel DOMMouseScroll')
		}
	}

	function autoScroll(direction, obj){
		bindWheel()
		var targetTop = 0;		
		if(direction == 'down'){
			if($(obj).attr('id') == 'introMain'){
				targetTop = $('#businessMain').offset().top - 90;
			} else if($(obj).attr('id') == 'businessMain'){
				targetTop = $('#newsMain').offset().top - 90;
			}
		} else {
			if($(obj).attr('id') == 'businessMain'){				
				if($(document).scrollTop() > $('#businessMain').offset().top - 90) {
					targetTop = $('#businessMain').offset().top - 90;
				} else {
					targetTop = 0;
				}
			}
		}		
		$('html, body').stop().animate({'scrollTop': targetTop}, 320)
		bindWheel(true)		
	}
}

function scrollEvent(){
	var minMargin = $(window).height()/3;
	var newsMainTop = $('#newsMain').offset().top - minMargin;
	var recruitMainTop = $('#recruitMain').offset().top - minMargin;	
	$(document).on({
		'scroll' : function(e){
			var nowScroll = $(document).scrollTop();			
			if(nowScroll > newsMainTop) {
				$('#newsMain').removeClass('readyMotion');
			}
			if(nowScroll > recruitMainTop) {
				$('#recruitMain').removeClass('readyMotion');
			}
		}
	})
}