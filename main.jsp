<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/mob/kor/include/declare.jspf"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<%@ include file="/WEB-INF/jsp/mob/kor/include/header.jspf"%>
<script>
var popupCnt = '<c:out value="${fn:length(popupList) }"/>';
$(document).ready(function() {
	<c:if test="${not empty popupList }">
		<c:forEach items="${popupList }" var="result" varStatus="status">
			var _seq = "<c:out value="${result.SEQ}"/>";
			var _cookieNm = "modal" + _seq;
			if (getCookie(_cookieNm) && getCookie(_cookieNm) == "hide") {
				$('#popupBtn' + _seq).click();
				popupCnt--;
			}
			if(popupCnt < 2){
				$('.modal').removeClass('overTwice');
			}
		</c:forEach>
	</c:if>
});


function fnPopupNotToday(_seq, obj){
	var modalNm = "modal" + _seq;
	if ($('#'+modalNm).is(":checked") == true) { 
	    setCookie(modalNm, "hide", 1);
	}
	modalClose(obj);	
}

function setCookie(name, value, expiredays) //쿠키 저장함수
{
    var todayDate = new Date();
    todayDate.setDate(todayDate.getDate() + expiredays);
    document.cookie = name + "=" + escape(value) + "; path=/; expires="
            + todayDate.toGMTString() + ";"
}

function getCookie(Name) { // 쿠키 불러오는 함수
    var search = Name + "=";
    if (document.cookie.length > 0) { // if there are any cookies
        offset = document.cookie.indexOf(search);
        if (offset != -1) { // if cookie exists
            offset += search.length; // set index of beginning of value
            end = document.cookie.indexOf(";", offset); // set index of end of cookie value
            if (end == -1)
                end = document.cookie.length;
            return unescape(document.cookie.substring(offset, end));
        }
    }
}
</script>
</head>
<body class="mainPage">
<%@ include file="/WEB-INF/jsp/mob/kor/include/gnb.jspf"%>
	<section id="mainSlide" class="slidingMainVisual">
		<button type="button" class="prev">이전 이미지</button>
		<button type="button" class="next">다음 이미지</button>
		<div>
			<ul>
				<!-- 이미지 1개 단위 시작 -->
				<!-- main image는 background-image: url('root+imageName')으로 표시 -->
				<!-- 기본 사이즈 1920 X 1080px -->
				<c:forEach var="result" items="${bannerList}" varStatus="status">
				<li data-seq="<c:out value="${result.SEQ }"/>" style="background-image: url('<c:out value="${result.M_IMG_DIR }"/>')" class="off">
					<hgroup>
						<!-- 게시글 타이틀 -->
						<h1><span><c:out value="${result.TITLE }"/></span></h1>
						<!-- 게시글 서브타이틀 -->
						<h2><span><c:out value="${result.SUB_TITLE }"/></span></h2>
					</hgroup>
					<!-- 게시글 링크 이동 top.location.href 처리 -->
					<c:if test="${result.LINK_YN eq 'Y'}">
						<button type="button" onclick="window.open('<c:out value="${result.LINK_URL }"/>')">VIEW MORE</button>
					</c:if>
					<c:if test="${result.LINK_YN eq 'O'}">
						<button type="button" onclick="location.href='<c:out value="${result.LINK_URL }"/>';">VIEW MORE</button>
					</c:if>
				</li>
				</c:forEach>
			</ul>			
		</div>
		<!-- slide Nav -->
		
		<ol class="indicator">
		</ol>
		<script type="text/javascript">			
			mainImgMove(); 
			//swipeMove('slidingMainVisual', true);
			//fullIndicatorAct('slidingMainVisual')
		</script>
		<!-- //slide Nav -->
	</section>
	<section id="mainProduct">
		<ul>
			<!-- data/productTree.json parse -->
		</ul>
	</section>
	<section id="mainNews" class="slidingMainNews">
		<button type="button" class="newsPrev">이전목록</button>
		<button type="button" class="newsNext">다음목록</button>
		<h1>Nex1 NEWS</h1>		
		<div>
			<ul>
				<!-- news List 시작 -->
				<c:forEach var="result" items="${newsList}" varStatus="status">
					<%-- <c:choose>
						<c:when test="${result.LINK_YN eq 'Y'}">
							<li onclick="window.open('<c:out value="${result.LINK_URL }"/>')'">
						</c:when>
						<c:when test="${result.LINK_YN eq 'O'}">
							<li onclick="location.href='<c:out value="${result.LINK_URL }"/>'">
						</c:when>
						<c:otherwise> --%>
							<li onclick="location.href='/web/kor/prcenter/news/view.do?seq=<c:out value="${result.SEQ }"/>';">
						<%-- </c:otherwise>
					</c:choose> --%>
						<!-- img size : 510 X 340-->
						<div>
							<div style="background-image: url('<c:out value="${result.THMBN_DIR }"/>')" title="<c:out value="${result.THMBN_NM }"/>"></div>
							<div></div>
						</div>
						<hgroup>
							<!-- 분류 -->
							<h3><c:out value="${result.GUBUN_NM }"/></h3>
							<!-- 제목 -->
							<h1><c:out value="${result.TITLE }"/></h1>
							<!-- 작성일 -->
							<h2><c:out value="${result.VIEW_DT }"/></h2>
						</hgroup>
					</li>
				</c:forEach>
				<!-- news List 끝 -->
			</ul>
		</div>
		<script type="text/javascript">
			// 06. 좌우 슬라이드 메뉴 type2
			slideMove ('slidingMainNews', 1) 
			swipeMove ('slidingMainNews', true);
		</script>
	</section>
	<section id="mainRecruit">
		<h1>인재채용</h1>
		<h2>LIG넥스원은 공정하고 체계적인 절차에 따라<br>인재를 선발합니다</h2>
		<ul>
			<li>
				<a href="/web/kor/recruit/talent.do">인사제도</a>		
			</li>
			<li>
				<a href="/web/kor/recruit/introduction/list.do">직무소개</a>		
			</li>
			<li>
				<a href="/web/kor/recruit/recuritmenttype.do">채용전형</a>		
			</li>
			<li>
				<a href="/web/kor/recruit/jobannouncement/list.do">채용공고</a>
			</li>
		</ul>
	</section>
	<section id="mainEtcMenu">
		<ul>
			<li><a href="/web/kor/prcenter/newsletter.do">웹진 근두운</a></li>
			<li><a href="/web/kor/util/customerQuetion.do">고객문의</a></li>
			<li><a href="/web/kor/util/contactUs.do">Contact Us</a></li>			
		</ul>
	</section>
	<%@ include file="/WEB-INF/jsp/mob/kor/include/footer.jspf"%>
	<!--  메인 모달 영역 -->
	<!--  클래스 정의 모달이 1개일 시 class="modal" -->
	<!--  클래스 정의 모달이 2개 이상 일시 class="modal overTwice" -->
	<c:forEach var="result" items="${popupList}" varStatus="status">
		<div class="modal <c:if test="${fn:length(popupList) gt 1 }">overTwice</c:if>">
			<div class="modalContent">
				<!-- 이미지 컨텐츠 일 시 h1, div선언 없이 바로 이미지 및 img 노출 -->				
				<c:choose>
					<c:when test="${result.GUBUN eq 'T' }">
						<c:choose>
							<c:when test="${result.LINK_YN eq 'Y'}">
								<div onclick="window.open('<c:out value="${result.LINK_URL }"/>')'">
							</c:when>
							<c:when test="${result.LINK_YN eq 'O'}">
								<div onclick="location.href='<c:out value="${result.LINK_URL }"/>'">
							</c:when>
							<c:otherwise>
								<div>
							</c:otherwise>
						</c:choose> 
							<h1><c:out value="${result.TITLE }"/></h1>
							<div>
								${result._contents }
							</div>
						</div>
					</c:when>
					<c:otherwise>
						<c:choose>
							<c:when test="${result.LINK_YN eq 'Y'}">
								<a href='<c:out value="${result.LINK_URL }"/>' target="_blank">
							</c:when>
							<c:when test="${result.LINK_YN eq 'O'}">
								<a href='<c:out value="${result.LINK_URL }"/>'>
							</c:when>
							<c:otherwise>
								<a>
							</c:otherwise>
						</c:choose> 
							<img src="<c:out value="${result.IMG_DIR }"/>" alt="<c:out value="${result._img_tx }"/>">							
						</a>
					</c:otherwise>
				</c:choose>
			</div>
			<div class="modalControl">
				<!-- 인풋 카운트 필요 id="modal + num" -->
				<input type="checkBox" id="modal<c:out value="${result.SEQ }"/>" onchange="fnPopupNotToday('<c:out value="${result.SEQ }"/>', this);">
				<!-- 인풋 카운트 필요 for="modal + num" -->
				<label for="modal<c:out value="${result.SEQ }"/>">오늘 하루 열지 않기</label>
				<button type="button" id="popupBtn<c:out value="${result.SEQ }"/>" onclick="modalClose(this);">창닫기</button>
			</div>		
		</div>
	</c:forEach>
</body>	
</html>